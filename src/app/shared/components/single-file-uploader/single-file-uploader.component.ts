import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FileService} from 'src/app/core/services/file.service';
import {AlertService} from '../../../core/services/alert.service';

@Component({
  selector: 'app-single-file-uploader',
  templateUrl: './single-file-uploader.component.html',
  styleUrls: ['./single-file-uploader.component.scss']
})
export class SingleFileUploaderComponent implements OnInit {
  @Input() choiceLabel = 'Deal.PaperDeal.Upload';
  @Input('formats') formats: string;
  @Output() fileId: EventEmitter<any> = new EventEmitter<any>();
  @Output() fileName: EventEmitter<any> = new EventEmitter<any>();

  formData: FormData = new FormData();
  filename: string;
  isUploading = false;
  fileIsUploading = 'Идет загрузка';
  dotsProgressBar = '';

  constructor(private _fileService: FileService,
              private _alertService: AlertService) {
  }

  ngOnInit() {
  }

  setFile(filename, fileGuid) {
    this.filename = filename;
    this.formData.delete(filename);
    this.isUploading = false;
    this.fileId.emit(fileGuid);
    this.fileName.emit(filename);
  }

  clearFile() {
    this.filename = null;
    this.isUploading = false;
    this.fileId.emit(null);
    this.fileName.emit(null);
  }

  /**
   * Загрузка одного файла
   * @param evt
   */
  onSelectFile(evt: any) {
    this.startProgressBar();
    this.choiceLabel = '';
    const filename = evt.files[0].name;
    this.formData.append(filename, evt.files[0]);
    this._fileService.post(this.formData).subscribe(success => {
        this.filename = filename;
        this.formData.delete(filename);
        this.isUploading = false;
        this.fileId.emit(success[0].id);
        this.fileName.emit(filename);
      },
      error => {
        this.OnErrorResult(error.error);
        this.isUploading = false;
      });
  }

  startInterval() {
    const interval = setInterval(() => {
      if (this.dotsProgressBar.length > 3) {
        this.dotsProgressBar = '';
      } else {
        this.dotsProgressBar += '.';
      }
      if (!this.isUploading) {
        clearInterval(interval);
      }
    }, 150);
  }

  startProgressBar() {
    this.isUploading = true;
    this.startInterval();
  }

  OnErrorResult(message: string) {
    this._alertService.error(message);
  }

  getFormats(): string {
    if (this.formats) {
      return this.formats;
    }
  }
}
