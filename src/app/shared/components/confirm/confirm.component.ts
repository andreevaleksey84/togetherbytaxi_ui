import {NgbActiveModal} from '@ng-bootstrap/ng-bootstrap';
import {Component} from '@angular/core';

@Component({
    selector: 'app-confirm',
    templateUrl: './confirm.component.html',
    styleUrls: ['./confirm.component.css']
})
export class ConfirmComponent {

    constructor(private _activeModal: NgbActiveModal){}

    accept(){
        this._activeModal.close(true);
    }

    cancel(){
        this._activeModal.close(false);
    }
}
