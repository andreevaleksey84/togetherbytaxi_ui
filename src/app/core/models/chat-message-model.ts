export class ChatMessageModel {
    public clientuniqueid: string;
    public type: string;
    public message: string;
    public date: Date;    
}
