import {Component, EventEmitter, forwardRef, Input, Output} from '@angular/core';
import {ControlValueAccessor, NG_VALUE_ACCESSOR} from '@angular/forms';
import {tap} from 'rxjs/internal/operators/tap';
import {map} from 'rxjs/operators';
import {FileService} from 'src/app/core/services/file.service';

@Component({
    selector: 'app-file-uploader-reactive',
    templateUrl: './file-uploader-reactive.component.html',
    styleUrls: ['./file-uploader-reactive.component.scss'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => FileUploaderReactiveComponent),
            multi: true
        }
    ]
})
export class FileUploaderReactiveComponent implements ControlValueAccessor {
    @Input('value') val: string = null;
    @Input() choiceLabel = 'Deal.PaperDeal.Upload';
    @Input() formats = 'application/vnd.openxmlformats-officedocument.wordprocessingml.document';
    @Input() isTemporaryFile = false;
    @Output() setFileId = new EventEmitter<string>();
    name = '';
    isUploading = false;

    constructor(private _fileService: FileService) {
    }

    get value() {
        return this.val;
    }

    set value(val) {
        this.val = val;
        this.onChange(val);
        this.onTouched();
    }

    onChange: any = () => {
    };

    onTouched: any = () => {
    };

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    writeValue(val) {
        this.setFileId.emit(val);
        this.value = val;
    }

    upload(value) {
        this.isUploading = true;
        const filename = value.files[0].name;
        this._fileService.post(this.createFormData(filename, value.files[0])).pipe(
            map(files => files[0].id),
            tap(id => this.writeValue(id)),
            tap(() => this.name = filename),
            tap(() => this.isUploading = false)
        ).subscribe();
    }

    createFormData(filename: string, file: File) {
        const formData = new FormData();
        formData.append(filename, file);
        return formData;
    }

    clear = () => this.writeValue(null);

    fileUrl = (id) => this.isTemporaryFile ? this._fileService.getPreSaveUrlBy(id) : this._fileService.getUrlBy(id);
}
