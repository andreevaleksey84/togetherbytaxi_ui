import { EventEmitter, Injectable } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@aspnet/signalr';
import { ChatMessageModel } from '../models/chat-message-model';
import {AppConfig} from 'src/app/app.config';
import { ChatDirectMessageModel } from '../models/chat-direct-message-model';
import { AuthService } from './auth.service';

@Injectable()
export class ChatService {
    messageReceived = new EventEmitter<ChatMessageModel>();
    directMessageReceived = new EventEmitter<ChatDirectMessageModel>();
    connectionEstablished = new EventEmitter<Boolean>();

    private connectionIsEstablished = false;
    private _hubConnection: HubConnection;

    constructor(private _authService: AuthService) {
        this.createConnection();
        this.registerOnServerEvents();
        this.startConnection();
    }

    sendMessage(message: ChatMessageModel) {
        this._hubConnection.invoke('NewMessage', message);
    }

    sendMessageWithText(text: string) {
        if(!this._authService.isAuthorized){
            console.error('Сообщение может отправлять только авторизованный пользователь');
            return;
        }
        if(!this.connectionIsEstablished){
            console.error('Сообщение не может быть отправлено, так как не подключения к серверу');
            return;
        }
        var currectUserId = this._authService.currentUser.userId;
        console.log('currectUserId', currectUserId);
        let message = new ChatMessageModel();
        message.clientuniqueid = currectUserId;
        message.message = text;
        message.type = "sent";
        message.date = new Date();
        this._hubConnection.invoke('NewMessage', message);
    }

    sendDirectMessage(message: ChatDirectMessageModel) {
        this._hubConnection.invoke('DirectMessage', message);
    }

    sendDirectMessageWithText(text: string, to: string) {
        if(!this._authService.isAuthorized){
            console.error('Сообщение может отправлять только авторизованный пользователь');
            return;
        }
        if(!this.connectionIsEstablished){
            console.error('Сообщение не может быть отправлено, так как не подключения к серверу');
            return;
        }
        var currectUserId = this._authService.currentUser.userId;
        console.log('currectUserId', currectUserId);
        let message = new ChatDirectMessageModel();
        message.clientidfrom = currectUserId;
        message.clientidto = to;
        message.message = text;
        message.type = "sent";
        message.date = new Date();
        this._hubConnection.invoke('DirectMessage', message);
    }

    private createConnection() {
        this._hubConnection = new HubConnectionBuilder()
            .withUrl(`${AppConfig.settings.signalR.url}/messagehub`)
            .build();
    }

    private startConnection(): void {
        let that = this;
        this._hubConnection
            .start()
            .then(() => {
                this.connectionIsEstablished = true;
                console.log('Hub connection started');
                this.connectionEstablished.emit(true);
            })
            .catch(err => {
                console.log('Error while establishing connection, retrying.', err);
                console.error('Error while establishing connection, retrying...');
                setTimeout(function () { that.startConnection(); }, 5000);
            });
    }

    private registerOnServerEvents(): void {
        this._hubConnection.on('MessageReceived', (data: any) => {
            this.messageReceived.emit(data);
        });
        this._hubConnection.on('DirectMessageReceived', (data: any) => {
            this.directMessageReceived.emit(data);
        });
    }
}