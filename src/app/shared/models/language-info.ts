export class LanguageInfo {
    constructor(c: string, n: string) {
        this.code = c;
        this.name = n;
    }
    code: string;
    name: string;
}
