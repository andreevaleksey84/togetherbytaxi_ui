import { Component, OnInit, OnDestroy } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from '../../../core/services/auth.service';
import {Constants} from '../../../core/consts/constants';
import { Subject } from 'rxjs';
import {LanguageInfo} from '../../models/language-info';

@Component({
    selector: 'app-navbar',
    templateUrl: './navbar.component.html',
    styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit, OnDestroy {

    public supportedLanguages: LanguageInfo[];
    private unsubscribe: Subject<any> = new Subject<any>();

    constructor(private _authService: AuthService,
                private constants: Constants,
                private _translateService: TranslateService) {
        this.supportedLanguages = this.constants.SupportedLanguages;
        this.onSelectLanguage(this.constants.DefaultLanguage);
    }

    ngOnInit() {
    }

    ngOnDestroy() {
        this.unsubscribe.next();
        this.unsubscribe.complete();
    }

    public hasPermissionStartsWith(permissionStart: string): boolean {
        return this._authService.hasPermissionStartsWith(permissionStart);
    }

    public isAuthorized(): boolean {
        return this._authService.isAuthorized;
    }

    public onSelectLanguage(newValue) {
        this._translateService.use(newValue);
    }
}
