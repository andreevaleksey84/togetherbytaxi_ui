import { Component, OnInit, Input } from '@angular/core';

@Component({
    selector: 'app-banner',
    templateUrl: './banner.component.html',
    styleUrls: ['./banner.component.scss']
})
export class BannerComponent implements OnInit {
    @Input() images: any[] = [
        {source: 'assets/images/banners/banner1.jpg', alt: 'Temp banner', title: ''},
        {source: 'assets/images/banners/banner2.jpg', alt: 'Temp banner', title: ''},
        {source: 'assets/images/banners/banner3.jpg', alt: 'Temp banner', title: ''}
    ];

    public gallerySettings = {
        panelWidth: 1170,
        panelHeight: 150,
        frameWidth: 6,
        frameHeight: 6,
        showCaption: false,
        autoPlay: true,
        transitionInterval: 15000
    };
    constructor() {}

    ngOnInit() {
    }
}
