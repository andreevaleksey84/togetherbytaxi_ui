import {UserExternalLoginInfo} from 'src/app/shared/models/user-external-login-info';

export class UserInfo {
  id: string;
  idFromIt1: string;
  name: string;
  email: string;
  emailConfirmed: boolean;
  phone: string;
  phoneConfirmed: boolean;
  userExternalLogins: Array<UserExternalLoginInfo>;
}
