import {LocationInfoModel} from './location-info-model';

export class OrderInfoModel {
    public orderId: string;
    public number: number;
    public from: LocationInfoModel;
    public to: LocationInfoModel;
    public date: Date;
}
