export class Pager {
  TotalRecords?: number;
  CurrentPage?: number;
  TotalPages?: number;
  RecordsVisible?: number;
  LoadMoreVisible?: boolean;
}
