import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { TranslateModule } from '@ngx-translate/core';
import { AngularSvgIconModule } from 'angular-svg-icon';
import { OwlDateTimeIntl, OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {
    AccordionModule,
    CalendarModule,
    CheckboxModule,
    DialogModule,
    DropdownModule,
    FileUploadModule,
    GalleriaModule,
    InputTextareaModule,
    InputTextModule,
    RadioButtonModule,
    TooltipModule,
    TreeModule,
} from 'primeng/primeng';
import {
    DateTimePickerReactiveComponent,
} from 'src/app/shared/components/date-time-picker-reactive/date-time-picker-reactive.component';
import {
    FileUploaderReactiveComponent,
} from 'src/app/shared/components/file-uploader-reactive/file-uploader-reactive.component';
import { FormBlockComponent } from 'src/app/shared/components/form-block/form-block.component';
import { BannerComponent } from './components/banner/banner.component';
import { ConfirmComponent } from './components/confirm/confirm.component';
import { CustomAccordionComponent } from './components/custom-accordion/custom-accordion.component';
import { DefaultIntl } from './components/date-time-picker-reactive/date-time-intl';
import { DateTimePickerComponent } from './components/date-time-picker/date-time-picker.component';
import { FileNameViewerComponent } from './components/file-name-viewer/file-name-viewer.component';
import { HeaderComponent } from './components/header/header.component';
import { LayoutComponent } from './components/layout/layout.component';
import { LoginCallbackComponent } from './components/login-callback/login-callback.component';
import { AddUserExternalSystemAccountComponent } from './components/login/add-user-external-system-account.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/login/register.component';
import { MultiplyFileUploaderComponent } from './components/multiply-file-uploader/multiply-file-uploader.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { SingleFileUploaderComponent } from './components/single-file-uploader/single-file-uploader.component';
import { TooltipComponent } from './components/tooltip/tooltip.component';
import { TruncatePipe } from './pipes/truncate.pipe';
import { MultiplyFileUploaderNewComponent } from './components/multiply-file-uploader-new/multiply-file-uploader-new.component';
import { NumericNamesPipe } from './pipes/numeric-names.pipe';
import {DateMscPipe} from './pipes/date-msc.pipe';

@NgModule({
    imports: [
        CommonModule,
        RouterModule,
        TranslateModule,
        FormsModule,
        NgbModule,
        CalendarModule,
        AccordionModule,
        DropdownModule,
        RadioButtonModule,
        CheckboxModule,
        FileUploadModule,
        DialogModule,
        FlexLayoutModule,
        ReactiveFormsModule,
        InputTextModule,
        InputTextareaModule,
        AngularSvgIconModule,
        TooltipModule,
        OwlDateTimeModule,
        OwlNativeDateTimeModule,
        TreeModule,
        GalleriaModule,
    ],
    declarations: [
        LayoutComponent,
        HeaderComponent,
        NavbarComponent,
        LoginComponent,
        AddUserExternalSystemAccountComponent,
        LoginCallbackComponent,
        DateTimePickerComponent,
        RegisterComponent,
        CustomAccordionComponent,
        SingleFileUploaderComponent,
        MultiplyFileUploaderComponent,
        ConfirmComponent,
        BannerComponent,
        TooltipComponent,
        FormBlockComponent,
        DateTimePickerReactiveComponent,
        FileUploaderReactiveComponent,
        FileNameViewerComponent,
        TruncatePipe,
        MultiplyFileUploaderNewComponent,
        NumericNamesPipe,
        DateMscPipe,
    ],
    exports: [
        LayoutComponent,
        LoginComponent,
        AddUserExternalSystemAccountComponent,
        LoginCallbackComponent,
        SingleFileUploaderComponent,
        MultiplyFileUploaderComponent,
        FormsModule,
        DateTimePickerComponent,
        AccordionModule,
        DropdownModule,
        RadioButtonModule,
        CheckboxModule,
        FileUploadModule,
        CustomAccordionComponent,
        ConfirmComponent,
        FlexLayoutModule,
        ReactiveFormsModule,
        InputTextModule,
        InputTextareaModule,
        NavbarComponent,
        HeaderComponent,
        InputTextareaModule,
        BannerComponent,
        AngularSvgIconModule,
        TooltipModule,
        TooltipComponent,
        FormBlockComponent,
        DateTimePickerReactiveComponent,
        FileUploaderReactiveComponent,
        TreeModule,
        GalleriaModule,
        FileNameViewerComponent,
        TruncatePipe,
        MultiplyFileUploaderNewComponent,
        NumericNamesPipe,
        DateMscPipe,
    ],
    entryComponents: [ ConfirmComponent],
    providers: [{ provide: OwlDateTimeIntl, useClass: DefaultIntl }],
})
export class SharedModule {}
