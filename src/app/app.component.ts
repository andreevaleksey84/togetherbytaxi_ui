import {Component, ChangeDetectorRef, AfterViewChecked, NgZone} from '@angular/core';
import { SpinnerService } from './core/services/spinner.service';
import { ChatDirectMessageModel } from './core/models/chat-direct-message-model';
import { ChatService } from './core/services/chat.service';
import { AlertService } from './core/services/alert.service';
import { AuthService } from './core/services/auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewChecked {
    constructor(
        public spinner: SpinnerService,
        private cdRef: ChangeDetectorRef,
        private _chatService: ChatService,
        private _ngZone: NgZone,
        private _alertService: AlertService,
        private _authService: AuthService,
        ) {
            this.subscribeToChatEvents();
        }

    ngAfterViewChecked() {
        this.cdRef.detectChanges();
    }
    
    private subscribeToChatEvents(): void {
        this._chatService.directMessageReceived.subscribe((message: ChatDirectMessageModel) => {
            this._ngZone.run(() => {
                if(this._authService.isAuthorized && message.clientidto == this._authService.currentUser.userId) {
                    if(message.message == 'RideTogether') {
                        this._alertService.rideTogether();
                        this._chatService.sendDirectMessageWithText("ReceivedRideTogether", message.clientidfrom);
                    } else if(message.message == 'ReceivedRideTogether') {
                        this._alertService.rideTogetherReceived();
                    }
                    else {
                        this._alertService.info('Вы получили сообщение от пользователя');
                    }
                }
            });
        });
    }
}
