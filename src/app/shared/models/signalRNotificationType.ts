export enum SignalRNotificationType {

  LotHasBeenPublished = 5,

  ErrorDuringLotPublish = 10,

  LotStateHasBeenChanged = 15,

  ErrorDuringLotStateChange = 20,

  ApplicationHasBeenPublished = 25,

  ErrorDuringApplicationPublish = 30,

  ApplicationStateHasBeenChanged = 35,

  ErrorDuringApplicationStateChange = 40,

  LotBidHasBeenAdded = 45,

  ErrorDuringAddingLotBid = 50,

  ProtocolHasBeenCreated = 55,

  LotsGridHasBeenUpdated = 60
}


