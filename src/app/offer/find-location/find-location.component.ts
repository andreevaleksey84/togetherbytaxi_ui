import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {ITakeUntilDestroy, TakeUntilDestroy} from '../../core/services/destroySubjects.decorator';
import {FindNearbyLocationResponse} from '../../core/models/find-nearby-location-response';
import {TranslateService} from '@ngx-translate/core';
import { AuthService } from 'src/app/core/services/auth.service';
import { LocationService } from 'src/app/core/services/location.service';
import { ILocation } from 'src/app/core/models/ilocation';
import { AlertService } from 'src/app/core/services/alert.service';

@Component({
    selector: 'app-find-location',
    templateUrl: './find-location.component.html',
    styleUrls: ['./find-location.component.scss']
})

@TakeUntilDestroy
export class FindLocationComponent implements OnInit, OnDestroy, ITakeUntilDestroy {
    public componentDestroy: any;
    @Input() locationToSearch: ILocation;
    @Input() buttonText: string;
    @Input() searchInLocationFrom: boolean;
    @Input() searchInLocationTo: boolean;
    nearByLocations: Array<FindNearbyLocationResponse>;
    searchPressed: boolean;

    constructor(
        private _translateService: TranslateService,
        private _authService: AuthService,
        private _locationService: LocationService,
        private _alertService: AlertService,
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    isAuthorized() {
        return this._authService.isAuthorized;
    }

    async findCompanionLocation() {
        try {
            this.searchPressed = true;
            this.nearByLocations = await this._locationService.findNearby(this.locationToSearch, this.searchInLocationFrom, this.searchInLocationTo).toPromise();
        } catch (error) {
            this.searchPressed = false;
            console.log('Error during findCompanionLocationFrom', error);
            this.showErrorOccurs();
        }
    }

    private showErrorOccurs() {
        const errorMessage = this._translateService.instant('GenerateOffer.Messages.SorryError');
        this._alertService.error(errorMessage);
    }
}
