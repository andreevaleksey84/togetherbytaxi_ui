export class TaxiListModel {
    public id: string;
    public code: string;
    public name: string;
    public description: string;
    public phones: string[];
    public site: string;
}
