import {LocationInfoModel} from './location-info-model';

export class FindNearbyLocationResponse {
    public fromLocation: LocationInfoModel;
    public toLocation: LocationInfoModel;
    public userId: string;
    public isAuthorizedUser: boolean;
    public createdDate: Date;
    public howLong: string;
    public distance: number;
    public sameLocation: boolean;
}
