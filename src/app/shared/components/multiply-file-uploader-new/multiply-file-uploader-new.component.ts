import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {FileAsData} from '../../models/file-as-data';
import {FileService} from '../../../core/services/file.service';
import {AlertService} from '../../../core/services/alert.service';

@Component({
  selector: 'app-multiply-file-uploader-new',
  templateUrl: './multiply-file-uploader-new.component.html',
  styleUrls: ['./multiply-file-uploader-new.component.scss']
})
export class MultiplyFileUploaderNewComponent {
    @Input() fileIds = '';
    @Input() formats = '*/*';
    @Output() setFileIds = new EventEmitter<string>();
    @Input() isTemporaryFile = true;
    maxAllowedFiles = 6;
    allFileInfo = Array<FileAsData>();

    constructor(
        private _fileService: FileService,
        private _alertService: AlertService,
    ) {
    }

    // Удаляет id файла из списка
    deleteFileId = (id) => {
        this.allFileInfo = this.allFileInfo.filter(i => i.id !== id);
        this.emitAllFileIds();
    };

    addFiles(files: Array<FileAsData>) {
        if (this.allFileInfo.length >= this.maxAllowedFiles) {
            this._alertService.warning('Добавить можно не более ' + this.maxAllowedFiles + ' файлов');
            return;
        }
        this.allFileInfo
            ? files.forEach(file => this.allFileInfo.length < this.maxAllowedFiles
            ? this.allFileInfo = this.allFileInfo.concat(file)
            : null)
            : this.allFileInfo = files;

        this.allFileInfo = this.allFileInfo.filter(id => !!id);
        this.emitAllFileIds();
    }

    emitAllFileIds() {
        const allFileIds = this.allFileInfo.map(s => s.id).toString();
        this.setFileIds.emit(allFileIds);
    }

    trackByFn = index => index;

    fileUrl = (id) => this.isTemporaryFile ? this._fileService.getPreSaveUrlBy(id) : this._fileService.getUrlBy(id);
}
