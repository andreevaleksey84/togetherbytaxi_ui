import {Component, Input, OnInit} from '@angular/core';
import {Accordion} from 'primeng/primeng';

@Component({
  selector: 'app-custom-accordion',
  templateUrl: './custom-accordion.component.html',
  styleUrls: ['./custom-accordion.component.css'],
  providers: [Accordion]
})

export class CustomAccordionComponent implements OnInit {

  @Input('title') title: string;
  @Input('selected') selected: boolean;
  @Input('className') className: string;
  arrowIsSelected = false;

  constructor() {
  }

  ngOnInit() {
    this.arrowIsSelected = this.selected;
  }

  toggleArrowState() {
    this.arrowIsSelected = !this.arrowIsSelected;
  }

  arrowState() {
    return this.arrowIsSelected ? 'arrow-up' : 'arrow-down';
  }

  getClassName() {
    if (this.className) {
      return `${this.className}`;
    }
  }

  onTabOpen() {
    this.toggleArrowState();
  }

  onTabClose() {
    this.toggleArrowState();
  }
}
