import {Injectable} from '@angular/core';
import {HubConnection, HubConnectionBuilder} from '@aspnet/signalr';
import {Subject} from 'rxjs';
import {UserInfoSignalRMessage} from 'src/app/shared/models/userInfoSignalRMessage';
import {AppConfig} from 'src/app/app.config';
import {AlertService} from 'src/app/core/services/alert.service';

import {AuthService} from 'src/app/core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class SignalrService {

  public isConnected: boolean;
  private userHubConnection: HubConnection;

  public connectionEstablished = new Subject<Boolean>();
  public userInfo = new Subject();

  constructor(private _authService: AuthService,
              private _alertService: AlertService) {
    this.isConnected = false;

    if (this._authService.isAuthorized) {
      this.run();
    } else {
      this._authService.userLoggedIn.subscribe(() => {
        if (!this.isConnected) {
          this.run();
        }
      });
    }
  }

  private run() {
    this.createConnection();
    this.registerOnServerEvents();
    this.startConnection();
  }

  private createConnection() {
    this.userHubConnection = new HubConnectionBuilder()
      .withUrl(`${AppConfig.settings.signalR.url}/userhub`, {
        accessTokenFactory: () => this._authService.getToken()
      })
      .build();
  }

  private startConnection(): void {
    this.userHubConnection
      .start()
      .then(() => {
        console.log('Hub connection started');
        this.isConnected = true;
        this.connectionEstablished.next(true);
      })
      .catch(err => {
        console.log('Error while establishing connection, retrying...' + err);
        setTimeout(this.startConnection, AppConfig.settings.signalR.retry.timeout);
      });
  }

  private registerOnServerEvents(): void {
    this.userHubConnection.on('connected', (data: string) => {
      console.log(data);
      // this._alertService.show(data);
    });

    this.userHubConnection.on('disconnected', (data: string) => {
      console.log(data);
      // this._alertService.show(data);
    });

    this.userHubConnection.on('userInfo', (data: UserInfoSignalRMessage) => {
      this.userInfo.next(data);
    });
  }
}
