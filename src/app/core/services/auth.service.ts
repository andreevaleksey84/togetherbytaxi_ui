import {Injectable} from '@angular/core';
import {Router} from '@angular/router';
import {OidcSecurityService} from 'angular-auth-oidc-client';
import {Subject} from 'rxjs';

import {User} from 'src/app/shared/models/user';
import {AlertService} from 'src/app/core/services/alert.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  public isAuthorized: boolean;
  public currentUser: User;
  public userLoggedIn = new Subject();

  constructor(private _oidcSecurityService: OidcSecurityService,
              private _alertService: AlertService,
              private _router: Router) {
    this.isAuthorized = false;

    this._oidcSecurityService.getIsAuthorized()
      .subscribe(isAuthorized => {
        this.isAuthorized = isAuthorized;
        if (isAuthorized) {
          this.userLoggedIn.next();
          // this._alertService.success('Вы успешно вошли в систему');
          this._oidcSecurityService.getUserData().subscribe(userData => {
            console.log('userData');
            console.log(userData);
            this.currentUser = new User(
              userData.sub,
              userData.preferred_username,
              userData.given_name,
              userData.family_name,
              userData.email,
              userData.email_verified,
              Array.isArray(userData.permission) ? userData.permission : [userData.permission]
            );
            // console.log('authService.currentUser:' + JSON.stringify(this.currentUser));
            // console.log('authService.jwtToken:' + JSON.stringify(this.getToken()));
          },
            err => {
              console.log('error during getUserData');
              console.log(err);
            });
        }
      });
  }

  public login(redirectToRegister: boolean = false) {
    this._oidcSecurityService.setCustomRequestParameters({
      'redirect_to_register': redirectToRegister
    });
    this._oidcSecurityService.authorize();
  }

  public redirectToRegisterWindow() {
    this.login(true);
  }

  public logout() {
    this._oidcSecurityService.logoff();
  }

  public authorizedCallback() {
    this._oidcSecurityService.authorizedImplicitFlowCallback();
    const afterLogin = localStorage.getItem('backUrlAfterLogin');
    localStorage.setItem('backUrlAfterLogin', '');
    this._oidcSecurityService.onAuthorizationResult.subscribe(r => {
      console.log(r);
      if (afterLogin) {
        this._router.navigateByUrl(afterLogin);
      } else {
        this._router.navigateByUrl('/');
      }
    });
  }

  /**
   * Проверяет авторизован пользователь или нет
   */
  public getIsAuthorized() {
    return this._oidcSecurityService.getIsAuthorized();
  }

  /**
   * Пполучает данные пользователя
   */
  public getUserData() {
    return this._oidcSecurityService.getUserData();
  }

  /**
   * Возвращает токен доступа пользователя
   */
  public getToken(): string {
    return this._oidcSecurityService.getToken();
  }

  /**
   * Проверяет наличие переданного права доступа у пользователя
   * @param permission право доступа
   */
  public hasPermission(permission: string): boolean {
    return this.currentUser && this.currentUser.permissions && Array.isArray(this.currentUser.permissions) && this.currentUser.permissions.indexOf(permission) >= 0;
  }

    /**
     * Проверяет наличие переданного права доступа у пользователя
     * @param permission право доступа
     */
    public hasPermissionStartsWith(permissionLike: string): boolean {
        return this.currentUser &&
            this.currentUser.permissions &&
            Array.isArray(this.currentUser.permissions) &&
            this.currentUser.permissions.filter(item => item && item.indexOf(permissionLike) > -1).length > 0;
    }
}
