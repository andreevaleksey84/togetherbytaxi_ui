import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Component({
    selector: 'app-date-time-picker-reactive',
    templateUrl: './date-time-picker-reactive.component.html',
    styleUrls: ['./date-time-picker-reactive.component.css'],
    providers: [
        {
            provide: NG_VALUE_ACCESSOR,
            useExisting: forwardRef(() => DateTimePickerReactiveComponent),
            multi: true,
        },
    ],
})
export class DateTimePickerReactiveComponent implements ControlValueAccessor {
    @Input('value') val: Date;
    @Input() placeholder = 'Выберите дату и время';
    @Input() isDisabled = false;

    get value() {
        return this.val;
    }

    set value(val) {
        this.val = val;
        this.onChange(val);
        this.onTouched();
    }

    onChange: any = () => {};

    onTouched: any = () => {};

    registerOnChange(fn: any): void {
        this.onChange = fn;
    }

    registerOnTouched(fn: any): void {
        this.onTouched = fn;
    }

    writeValue(value) {
        if (value) this.val = value;
    }
}
