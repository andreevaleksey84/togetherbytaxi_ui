import {Component, Input, OnInit} from '@angular/core';

@Component({
  selector: 'app-yes-no-label',
  templateUrl: './yes-no-label.component.html',
  styleUrls: ['./yes-no-label.component.scss']
})
export class YesNoLabelComponent implements OnInit {
    @Input() value: boolean;
    @Input() noAdditionalText: string;

    constructor() {
    }

    ngOnInit() {
    }
}
