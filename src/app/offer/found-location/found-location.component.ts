import {Component, OnInit, OnDestroy, Input} from '@angular/core';
import {ITakeUntilDestroy, TakeUntilDestroy} from '../../core/services/destroySubjects.decorator';
import {FindNearbyLocationResponse} from '../../core/models/find-nearby-location-response';
import {TranslateService} from '@ngx-translate/core';
import { ILocation } from 'src/app/core/models/ilocation';

@Component({
    selector: 'app-found-location',
    templateUrl: './found-location.component.html',
    styleUrls: ['./found-location.component.scss'],
})

@TakeUntilDestroy
export class FoundLocationComponent implements OnInit, OnDestroy, ITakeUntilDestroy {
    public componentDestroy: any;
    @Input() foundLocations: Array<FindNearbyLocationResponse>;

    constructor(
        private _translateService: TranslateService,
    ) {
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    additional(distance: number): string {
        // (расстояние {{foundLocation.distance}} метров)
        return this._translateService.instant('FoundLocationsComponent.DistanceWithMetes', {distance: distance});
    }

    foundLocationTitle(foundLocation: FindNearbyLocationResponse): string {
        return 'Заказ создан ' + foundLocation.howLong + ' назад';
    }
}
