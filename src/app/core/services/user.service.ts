import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppConfig} from 'src/app/app.config';

import {UserInfo} from 'src/app/shared/models/user-info';

@Injectable({
  providedIn: 'root'
})

export class UserService {

  private requestHeader: HttpHeaders;
  private baseUrl = `${AppConfig.settings.openIDImplicitFlowConfiguration.issuer}/api/v1/user`;

  constructor(private _httpClient: HttpClient) {
    this.requestHeader = new HttpHeaders();
    this.requestHeader.append('Content-Type', 'application/json');
  }

  /**
   * Перенаправление на страницу добавления внешней системы авторизованному пользователю
   * @param requestUrl - Адрес запроса
   * @param returnUrl - Адрес возврата
   */
  public addExternalSystem(requestUrl: string, returnUrl: string) {
    const url = `${requestUrl}/account/edituserexternalsystemaccount?returnurl=` + returnUrl;
    window.location.href = url;
  }

  /**
   * Получение информации по пользователю
   */
  public getUserInfo(): Observable<UserInfo> {
    return this._httpClient.get<UserInfo>(`${this.baseUrl}/info`);
  }
}
