import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

import {IFilter} from 'src/app/shared/interfaces/filter.interface';
import {GridRefreshData} from '../models/grid-refresh-data';
import {GridRemoteData} from '../models/grid-remote-data';

@Injectable()
export class GridDataFlowService {

    public TriggerDataRefresh = new Subject<GridRefreshData>();
    public TriggerDataSearch = new Subject<IFilter>();
    public LoadedGridData = new Subject<GridRemoteData>();
    public GridLoadingFailed = new Subject<string>();

    constructor() {
    }

}
