export class LocationInfoModel {
    public lat: string;
    public lng: string;
    public address: string;
}
