export class CertificateInfo {
  id: string;
  thumbprint: string;
  idFromIt1: string;
}
