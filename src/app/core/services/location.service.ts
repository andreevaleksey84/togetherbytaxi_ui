import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppConfig} from 'src/app/app.config';
import { FindNearbyLocationModel } from '../models/find-nearby-location-model';
import { ILocation } from '../models/ilocation';
import {FindNearbyLocationResponse} from '../models/find-nearby-location-response';

@Injectable({
    providedIn: 'root'
})
export class LocationService {

    private requestHeader: HttpHeaders;
    private readonly baseUrl = `${AppConfig.settings.orderServiceUrl}/api/v1/location`;

    constructor(private _httpClient: HttpClient) {
        this.requestHeader = new HttpHeaders();
        this.requestHeader.append('Content-Type', 'application/json');
    }

    /**
     * Поискать точки заказов рядом
     * @param findNearbyLocationModel
     */
    public findNearby(location: ILocation, searchInLocationFrom: boolean, searchInLocationTo: boolean): Observable<Array<FindNearbyLocationResponse>> {
        const findNearbyLocationModel = new FindNearbyLocationModel();
        findNearbyLocationModel.Latitude = location.lat;
        findNearbyLocationModel.Longitude = location.lng;
        findNearbyLocationModel.Distance = 1000; // distance in meters
        findNearbyLocationModel.SearchInLocationFrom = searchInLocationFrom;
        findNearbyLocationModel.SearchInLocationTo = searchInLocationTo;
        return this._httpClient.post<Array<FindNearbyLocationResponse>>(`${this.baseUrl}/findNearby`, findNearbyLocationModel);
    }
}
