import {Component, Input} from '@angular/core';
import {fadeAnimation} from 'src/app/shared/animations/fade.animation';

@Component({
    selector: 'app-form-block',
    templateUrl: './form-block.component.html',
    styleUrls: ['./form-block.component.scss'],
    animations: [fadeAnimation]
})
export class FormBlockComponent {
    @Input() text: string;
    @Input() opened = true;

    toggle = () => this.opened = !this.opened;
}
