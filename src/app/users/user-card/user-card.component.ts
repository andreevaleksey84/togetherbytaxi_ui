import {Component, OnInit} from '@angular/core';
import {Router} from '@angular/router';
import {TranslateService} from '@ngx-translate/core';
import {AuthService} from 'src/app/core/services/auth.service';
import {UserService} from 'src/app/core/services/user.service';
import {UserInfo} from 'src/app/shared/models/user-info';

@Component({
    selector: 'app-user-card',
    templateUrl: './user-card.component.html',
    styleUrls: ['./user-card.component.css']
})
export class UserCardComponent implements OnInit {
    dataIsEmpty = this._translateService.instant('Data.IsEmpty');
    userInfo: UserInfo;

    constructor(private _authService: AuthService,
                private _router: Router,
                private _userService: UserService,
                private _translateService: TranslateService) {
        this.userInfo = new UserInfo();
        this.getUserInfo();
    }

    ngOnInit() {
    }

    arrayIsNotEmpty(data: Array<any>) {
        return data && data.length > 0;
    }

    humanizeBooleanType(value: boolean) {
        return !value ? this._translateService.instant('Boolean.No') : this._translateService.instant('Boolean.Yes');
    }

    private getUserInfo() {
        this._userService.getUserInfo().subscribe(success => {
            this.userInfo = success;
        });
    }
}
