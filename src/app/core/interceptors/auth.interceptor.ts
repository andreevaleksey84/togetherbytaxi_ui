import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from 'src/app/core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthInterceptor implements HttpInterceptor {

  constructor(private _authService: AuthService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let requestToForward = req;

    const token = this._authService.getToken();
    if (token !== '' && !req.url.startsWith('/assets/i18n')) {
      const tokenValue = 'Bearer ' + token;
      requestToForward = req.clone({setHeaders: {'Authorization': tokenValue}});
    }

    return next.handle(requestToForward);
  }
}
