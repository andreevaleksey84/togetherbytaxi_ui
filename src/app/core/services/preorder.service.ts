import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppConfig} from 'src/app/app.config';
import { PreOrderCreateModel } from '../models/preorder-create-model';
import { PreOrderChangeModel } from '../models/preorder-change-model';

@Injectable({
    providedIn: 'root'
})
export class PreOrderService {

    private requestHeader: HttpHeaders;
    private readonly baseUrl = `${AppConfig.settings.orderServiceUrl}/api/v1/preorder`;

    constructor(private _httpClient: HttpClient) {
        this.requestHeader = new HttpHeaders();
        this.requestHeader.append('Content-Type', 'application/json');
    }

    /**
     * Сформировать предварительный заказ
     * @param preOrderCreateModel
     */
    public create(preOrderCreateModel: PreOrderCreateModel): Observable<any> {
        return this._httpClient.post<PreOrderCreateModel>(`${this.baseUrl}`, preOrderCreateModel);
    }

    /**
     * Отредактировать предварительный заказ
     * @param preOrderChangeModel
     */
    public change(preOrderChangeModel: PreOrderChangeModel): Observable<any> {
        return this._httpClient.put<PreOrderChangeModel>(`${this.baseUrl}/${preOrderChangeModel.preOrderChangeId}`, preOrderChangeModel);
    }
}
