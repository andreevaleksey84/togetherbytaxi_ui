import {SignalRNotificationType} from 'src/app/shared/models/signalRNotificationType';

export class UserInfoSignalRMessage {
  public message: string;
  public type: SignalRNotificationType;
}


