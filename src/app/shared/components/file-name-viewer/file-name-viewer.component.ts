import {Component, Input, OnInit} from '@angular/core';
import {Observable} from 'rxjs';
import {FileService} from 'src/app/core/services/file.service';

@Component({
    selector: 'app-file-name-viewer',
    templateUrl: './file-name-viewer.component.html',
    styleUrls: ['./file-name-viewer.component.css'],
})
export class FileNameViewerComponent implements OnInit {
    @Input() id: string;
    @Input() name: string;
    @Input() url: string;
    @Input() isTemporaryFile = false;
    fileObj$: Observable<{id: string; name: string}>;

    constructor(private fileService: FileService) {}

    ngOnInit() {
        if (this.id) {
            this.fileObj$ = this.isTemporaryFile
                ? this.fileService.getPreSaveFileNameBy(this.id)
                : this.fileService.getFileNameById(this.id);
        }
    }

    fileUrl = id => (this.isTemporaryFile ? this.fileService.getPreSaveUrlBy(id) : this.fileService.getFileById(id));
}
