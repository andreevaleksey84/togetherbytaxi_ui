export class FindNearbyLocationModel {
    public Latitude: number;
    public Longitude: number;
    public Distance: number;
    public SearchInLocationFrom: boolean;
    public SearchInLocationTo: boolean;
}
