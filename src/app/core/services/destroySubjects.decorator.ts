import { Subject } from 'rxjs';

export function TakeUntilDestroy(constructor: any) {
    const originalDestroy = constructor.prototype.ngOnDestroy;

    if (typeof originalDestroy !== 'function') {
    }

    constructor.prototype.componentDestroy = function () {
        this._takeUntilDestroy = this._takeUntilDestroy || new Subject();

        return this._takeUntilDestroy.asObservable();
    };

    constructor.prototype.ngOnDestroy = function () {
        if (originalDestroy && typeof originalDestroy === 'function')
            originalDestroy.apply(this, arguments);

        if (this._takeUntilDestroy) {
            this._takeUntilDestroy.next();
            this._takeUntilDestroy.complete();
        }
    };
}

export interface ITakeUntilDestroy {
    componentDestroy: any;
}
