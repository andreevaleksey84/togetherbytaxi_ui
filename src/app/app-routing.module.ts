import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AuthGuard} from './core/guards/auth.guard';
import {LoginCallbackComponent} from './shared/components/login-callback/login-callback.component';
import {AddUserExternalSystemAccountComponent} from './shared/components/login/add-user-external-system-account.component';
import {LoginComponent} from './shared/components/login/login.component';
import {RegisterComponent} from './shared/components/login/register.component';

const routes: Routes = [
    {path: 'login', component: LoginComponent},
    {path: 'register', component: RegisterComponent},
    {path: 'login/callback', component: LoginCallbackComponent},
    {path: 'external', component: AddUserExternalSystemAccountComponent},
    {path: '', loadChildren: './info/info.module#InfoModule'},
    {path: 'offer', loadChildren: './offer/offer.module#OfferModule'},
    {path: 'taxi', loadChildren: './taxi/taxi.module#TaxiModule'},
    {path: 'user', canActivate: [AuthGuard], loadChildren: './users/users.module#UsersModule'},
    {path: '**', loadChildren: './not-found/not-found.module#NotFoundModule'}
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: [AuthGuard]
})
export class AppRoutingModule {
}
