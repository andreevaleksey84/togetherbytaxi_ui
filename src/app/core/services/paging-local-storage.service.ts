import {Injectable} from '@angular/core';

import {Pager} from 'src/app/shared/models/pager';

@Injectable({
  providedIn: 'root'
})
export class PagingLocalStorageService {

  constructor() {
  }

  public save(pager: Pager, gridName: string) {
    localStorage.setItem(`grid-pager_${gridName}`, JSON.stringify({
      RecordsVisible: pager.RecordsVisible
    }));
  }

  public load(gridName: string) {
    const pager = new Pager();
    pager.RecordsVisible = 10;

    const pagerDto = JSON.parse(localStorage.getItem(`grid-pager_${gridName}`));

    if (pagerDto === null) {
      return pager;
    }

    pager.RecordsVisible = pagerDto.RecordsVisible;

    return pager;
  }
}

