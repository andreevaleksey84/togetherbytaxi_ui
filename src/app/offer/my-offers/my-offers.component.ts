import {Component, OnInit} from '@angular/core';
import {OrderService} from '../../core/services/order.service';
import {ITakeUntilDestroy, TakeUntilDestroy} from '../../core/services/destroySubjects.decorator';
import {takeUntil} from 'rxjs/operators';
import {OrderInfoModel} from '../../core/models/order-info-model';
import { AuthService } from 'src/app/core/services/auth.service';
declare var $: any;

@Component({
    templateUrl: './my-offers.component.html',
    styleUrls: ['./my-offers.component.scss']
})

@TakeUntilDestroy
export class MyOffersComponent implements OnInit, ITakeUntilDestroy {
    public componentDestroy: any;
    public orders: OrderInfoModel[];

    constructor(
        private _orderService: OrderService,
        private _authService: AuthService,
    ) {
    }

    ngOnInit() {
        this.getAllMyOrders();
    }

    private getAllMyOrders() {
        this._orderService.getAll()
            .pipe(takeUntil(this.componentDestroy()))
            .subscribe((data: OrderInfoModel[]) => {
                this.orders = data;
            });
    }

    isAuthorized() {
        return this._authService.isAuthorized;
    }
}
