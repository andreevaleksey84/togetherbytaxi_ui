export class FileAsData {
  public id: string;
  public name: string;
  public content: string;
}
