import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Observable} from 'rxjs';
import {TranslateService} from '@ngx-translate/core';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class LanguageInterceptor implements HttpInterceptor {

  constructor(private _translateService: TranslateService) {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    let requestToForward = req;
    const language = this._translateService.currentLang === '' ? this._translateService.defaultLang : this._translateService.currentLang;
    if (language) {
      requestToForward = req.clone({setHeaders: {'X-TogetherByTaxi-Language': language}});
    }

    return next.handle(requestToForward);
  }
}
