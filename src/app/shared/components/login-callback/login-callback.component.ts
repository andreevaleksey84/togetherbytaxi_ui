import {Component, OnInit} from '@angular/core';

import {AuthService} from '../../../core/services/auth.service';

@Component({
  template: '<p>{{ "Login.Success" | translate }}</p>'
})
export class LoginCallbackComponent implements OnInit {

  constructor(private _authService: AuthService) {
  }

  ngOnInit() {
    this._authService.authorizedCallback();
  }
}
