import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {NotFoundRoutingModule} from 'src/app/not-found/not-found-routing.module';

import {NotFoundComponent} from './not-found/not-found.component';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        NotFoundRoutingModule
    ],
    declarations: [NotFoundComponent]
})
export class NotFoundModule {
}
