import { Component, OnInit } from '@angular/core';
import {TaxiListService} from '../../core/services/TaxiListService';
import {AlertService} from '../../core/services/alert.service';
import {TranslateService} from '@ngx-translate/core';
import {takeUntil} from 'rxjs/operators';
import {ITakeUntilDestroy, TakeUntilDestroy} from '../../core/services/destroySubjects.decorator';
import {TaxiListModel} from '../../core/models/taxi-list-model';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})

@TakeUntilDestroy
export class ListComponent implements OnInit, ITakeUntilDestroy {
    public componentDestroy: any;
    public taxiList: TaxiListModel[];
    constructor(
      private _taxiListService: TaxiListService,
      private _alertService: AlertService,
      private _translateService: TranslateService,
    ) { }

    ngOnInit() {
        this.getAllTaxiList();
    }

    private getAllTaxiList(): void {
        this._taxiListService
            .getAll()
            .pipe(takeUntil(this.componentDestroy()))
            .subscribe((data: TaxiListModel[]) => {
                console.log('ListComponent getAllTaxiList data:', data);
                this.taxiList = data;
            }, error => {
                console.log('ListComponent getAllTaxiList error occurs:', error);
                this.showErrorOccurs();
            });
    }

    private showErrorOccurs() {
        const errorMessage = this._translateService.instant('GenerateOffer.Messages.SorryError');
        this._alertService.error(errorMessage);
    }
}
