import { CurrencyMaskConfig } from 'ng2-currency-mask/src/currency-mask.config';

export const CustomCurrencyMaskConfig: CurrencyMaskConfig = {
    align: 'left', // Выравнивание текста при вводе. (default: right)
    allowNegative: false, // Если true, можно ввести отрицательные значения. (default: true)
    decimal: ',', // Разделитель десятичных знаков (default: '.')
    precision: 2, // Количество десятичных знаков (default: 2)
    prefix: '', // Денежный префикс (default: '$ ')
    suffix: '', // Денежный суффикс (default: '')
    thousands: ' ' // Разделитель тысяч (default: ',')
};
