import {Injectable} from '@angular/core';
import {ToastrService} from 'ngx-toastr';

@Injectable({
  providedIn: 'root'
})
export class AlertService {

  constructor(private _toastr: ToastrService) {
  }

  public show(message?: string, title?: string, options?) {
    this._toastr.show(message, title, options);
  }

  public success(message?: string, title?: string) {
    this._toastr.success(message, title);
  }

  public error(message?: string, title?: string) {
    this._toastr.error(message, title);
  }

  public info(message?: string, title?: string) {
    this._toastr.info(message, title);
  }

  public warning(message?: string, title?: string) {
    this._toastr.warning(message, title);
  }

  public rideTogether() {
    this._toastr.show('Пользователь приглашает вас поехать вместе на такси', 'Вас зовут к совместной поездке', {
      closeButton: true,
      disableTimeOut: true,
      positionClass: 'toast-top-center'
    });
  }

  public rideTogetherReceived() {
    this._toastr.show('Пользователь получил ваше приглашение', 'Подтверждение получения сообщения', {
      closeButton: true,
      disableTimeOut: true,
      positionClass: 'toast-top-center'
    });
  }

  public rideTogetherNotReceived() {
    this._toastr.show('Пользователь в течении 5 секунд не ответил. Скорее всего он не получил ваше приглашение', 'Не получил сообщениe', {
      closeButton: true,
      disableTimeOut: true,
      positionClass: 'toast-top-center'
    });
  }
}
