import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {DataTableModule} from 'primeng/primeng';
import {TranslateModule} from '@ngx-translate/core';
import {GridDataFlowService} from './services/grid-dataflow.service';
import {GridComponent} from './components/grid/grid.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    DataTableModule,
    TranslateModule
  ],
  declarations: [
    GridComponent
  ],
  providers: [
    GridDataFlowService
  ],
  exports: [
    DataTableModule,
    GridComponent
  ]
})
export class GridModule {
}
