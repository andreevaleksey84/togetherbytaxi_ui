import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {GenerateComponent} from './generate/generate.component';
import {MyOffersComponent} from './my-offers/my-offers.component';
import {AuthGuard} from '../core/guards/auth.guard';

const routes: Routes = [
    {path: '', component: GenerateComponent},
    {path: 'my', canActivate: [AuthGuard], component: MyOffersComponent}
];

@NgModule({
    declarations: [],
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OfferRoutingModule {
}
