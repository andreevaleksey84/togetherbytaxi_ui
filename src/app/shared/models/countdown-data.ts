export class CountdownData {
    statusText: string;
    date: Date;

    constructor(
        statusText: string,
        date: Date
    ) {
        this.statusText = statusText;
        this.date = date;
    }
}
