import { Injectable, NgZone } from '@angular/core';
import { Observable } from 'rxjs';
import * as _ from 'lodash';
import {TranslateService} from '@ngx-translate/core';

interface IWindow extends Window {
    webkitSpeechRecognition: any;
    SpeechRecognition: any;
}

@Injectable()
// https://hassantariqblog.wordpress.com/2016/12/04/angular2-web-speech-api-speech-recognition-in-angular2/
export class SpeechRecognitionService {
    speechRecognition: any;

    constructor(
        private zone: NgZone,
        private _translateService: TranslateService,
    ) {
    }

    record(): Observable<string> {

        return new Observable(observer => {
            const { webkitSpeechRecognition }: IWindow = <IWindow>window;
            this.speechRecognition = new webkitSpeechRecognition();
            // this.speechRecognition = SpeechRecognition;
            this.speechRecognition.continuous = true;
            // this.speechRecognition.interimResults = true;
            this.setLanguage();
            this.speechRecognition.maxAlternatives = 1;

            this.speechRecognition.onresult = speech => {
                let term: '';
                if (speech.results) {
                    const result = speech.results[speech.resultIndex];
                    const transcript = result[0].transcript;
                    if (result.isFinal) {
                        if (result[0].confidence < 0.3) {
                            console.log('Unrecognized result - Please try again');
                        } else {
                            term = _.trim(transcript);
                            console.log('Did you said? -> ' + term + ' , If not then say something else...');
                        }
                    }
                }
                this.zone.run(() => {
                    observer.next(term);
                });
            };

            this.speechRecognition.onerror = error => {
                observer.error(error);
            };

            this.speechRecognition.onend = () => {
                observer.complete();
            };

            this.speechRecognition.start();
            console.log('Say something - We are listening!!!');
        });
    }

    stop() {
        if (this.speechRecognition) {
            this.speechRecognition.stop();
        }
    }

    DestroySpeechObject() {
        if (this.speechRecognition) {
            this.speechRecognition.stop();
        }
    }

    setLanguage() {
        if (this.speechRecognition) {
            const speechRecognitionLang = this._translateService.instant('SpeechRecognition.Lang');
            this.speechRecognition.lang = speechRecognitionLang;
            console.log('Current language - ' + speechRecognitionLang);
        }
    }
}
