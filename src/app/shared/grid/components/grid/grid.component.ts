import {AfterViewInit, ChangeDetectorRef, Component, ContentChildren, EventEmitter, Input, OnDestroy, OnInit, Output, ViewChild} from '@angular/core';
import {LazyLoadEvent} from 'primeng/api';
import {DataTable} from 'primeng/primeng';
import {Column} from 'primeng/shared';
import {Subscription} from 'rxjs';
import {RefreshDataType} from 'src/app/shared/enums/refresh-data.type.enum';
import {IFilter} from 'src/app/shared/interfaces/filter.interface';
import {Pager} from 'src/app/shared/models/pager';
import {AlertService} from '../../../../core/services/alert.service';
import {PagingLocalStorageService} from '../../../../core/services/paging-local-storage.service';
import {GridRefreshData} from '../../models/grid-refresh-data';
import {GridRemoteData} from '../../models/grid-remote-data';
import {GridDataFlowService} from '../../services/grid-dataflow.service';

@Component({
    selector: 'app-paginated-lazy-grid',
    templateUrl: './grid.component.html',
    styleUrls: ['./grid.component.scss']
})
export class GridComponent implements OnInit, OnDestroy, AfterViewInit {

    @ContentChildren(Column) columns: any = [];
    @ViewChild(DataTable) dataTable: DataTable;

    @Input() filter: IFilter;
    @Input() gridName: string;
    @Input() gridClassName: string;
    @Input() showPaging = true;
    @Output() onRowDblClick: EventEmitter<any> = new EventEmitter<any>();

    @Input() visibleBtnLoadmore: boolean = true;
    public records: Array<any> = [];
    public isLoading = false;
    public pager: Pager;
    public gridHasNoData: boolean;
    private _dataFlowType = RefreshDataType.Default;
    private _subscriptionOnDataRefresh: Subscription;
    private _subscriptionOnDataSearch: Subscription;
    private _subscriptionOnLoadingFailed: Subscription;

    constructor(private _gridDataFlowService: GridDataFlowService,
                private _pagerSaver: PagingLocalStorageService,
                private _changeDetectorRef: ChangeDetectorRef,
                private _alertService: AlertService) {
    }

    ngOnInit() {
        this.pager = this._pagerSaver.load(this.gridName);
        this._subscriptionOnDataRefresh = this._gridDataFlowService.LoadedGridData
            .subscribe((data: GridRemoteData) => {
                this.gridHasNoData = data.invdata.length < 1;
                this._onDataRefresh(data);
            });
        this._subscriptionOnDataSearch = this._gridDataFlowService.TriggerDataSearch
            .subscribe((filter: IFilter) => this._onSearch(filter));
        this._subscriptionOnLoadingFailed = this._gridDataFlowService.GridLoadingFailed
            .subscribe((message: string) => this._onLoadingError(message));

    }

    ngAfterViewInit() {
        this.dataTable.cols = this.columns;
        this.dataTable.ngAfterContentInit();
        this._changeDetectorRef.detectChanges();
    }

    ngOnDestroy() {
        this._subscriptionOnDataRefresh.unsubscribe();
        this._subscriptionOnDataSearch.unsubscribe();
        this._subscriptionOnLoadingFailed.unsubscribe();
    }

    paginate(e: LazyLoadEvent): void {
        this.isLoading = true;
        this.filter.Page = e.first === 0 ? 1 : (e.first / e.rows + 1);
        this.filter.ItemsPerPage = e.rows;
        this._dataFlowType = RefreshDataType.Default;
        this._gridDataFlowService.TriggerDataRefresh.next(new GridRefreshData(this.filter, RefreshDataType.Default));
    }

    loadMore(): void {
        if (this.filter.Page === this.pager.TotalPages) {
            return;
        }
        this.isLoading = true;
        this.filter.Page = this.filter.Page + 1;
        this._dataFlowType = RefreshDataType.LoadMore;
        this._gridDataFlowService.TriggerDataRefresh.next(new GridRefreshData(this.filter, RefreshDataType.LoadMore));
    }

    callbackSavePager(event: any): void {
        this.pager.RecordsVisible = event.rows;
        this._pagerSaver.save(this.pager, this.gridName);
    }

    public rowStyleClass(data: any): string {
        return data.hasOwnProperty('IsViewedStatus') && !data.IsViewedStatus ? 'row--unread' : '';
    }

    private _onSearch(filter: IFilter): void {
        this.isLoading = true;
        this._dataFlowType = RefreshDataType.Search;
        this._gridDataFlowService.TriggerDataRefresh.next(new GridRefreshData(this.filter, RefreshDataType.Search));
    }

    private _onDataRefresh(data: GridRemoteData) {
        this.pager.TotalRecords = data.totalrecords;
        this.pager.TotalPages = data.totalpages;
        switch (this._dataFlowType) {
            case RefreshDataType.Default:
            case RefreshDataType.Search:
                this.records = data.invdata.concat([]);
                this.pager.RecordsVisible = this.filter.ItemsPerPage;
                break;
            case RefreshDataType.LoadMore:
                this.records = this.records.concat(data.invdata);
                this.pager.RecordsVisible += data.invdata.length;
                break;
        }
        this.isLoading = false;
    }

    private _onLoadingError(error: string) {
        this.isLoading = false;
        this._alertService.error(error);
    }
}
