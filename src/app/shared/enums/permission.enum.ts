export const enum PermissionEnum {
    tradesCreate = 'trades.create',
    applicationsCreate = 'applications.create'
}
