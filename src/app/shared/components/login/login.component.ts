import {Component, OnInit} from '@angular/core';

import {AuthService} from '../../../core/services/auth.service';

@Component({
  template: '<p>{{ "Login.Redirect" | translate }}</p>'
})
export class LoginComponent implements OnInit {

  constructor(private _authService: AuthService) {
  }

  ngOnInit() {
    this._authService.login();
  }
}
