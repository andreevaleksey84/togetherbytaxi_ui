// типы ценового предложения
export const proposalTypes = [
    {label: 'По начальной цене', value: '5', id: 5},
    {label: 'По начальной цене + шаг аукциона', value: '10', id: 10}
];
