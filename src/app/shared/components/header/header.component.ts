import {Component} from '@angular/core';
import {Observable} from 'rxjs';
import {AuthService} from 'src/app/core/services/auth.service';
import {UserInfo} from 'src/app/shared/models/user-info';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})

export class HeaderComponent {
    isAuthorized$: Observable<boolean> = this.authService.getIsAuthorized();

    user$: Observable<UserInfo> = this.authService.getUserData();

    logout = () => this.authService.logout();

    constructor(private authService: AuthService) {
    }
}
