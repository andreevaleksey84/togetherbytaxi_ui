export interface IAppConfig {
    env: {
        name: string;
    };
    production: boolean;
    releaseVersion: string;
    fileServiceUrl: string;
    orderServiceUrl: string;
    yandexMapsUrl: string;
    openIDImplicitFlowConfiguration: {
        issuer: string;
        redirect_url: string;
        // Временно, пока не будет решен вопрос с 5-секундной задержкой
        // Для корректной может потребоваться изменение настроек IdentityServer4 в SecurityHeadersAttributes, связанных с iframe
        // И прописать silent_renew_url в redirect_uri клиента в Auth
        silent_renew: boolean;
        silent_renew_url: string;
        post_login_route: string;
        post_logout_redirect_uri: string;
        trigger_authorization_result_event: boolean;
        log_console_warning_active: boolean;
        log_console_debug_active: boolean;
        client_id: string;
        response_type: string;
        scope: string;
        // for IE
        max_id_token_iat_offset_allowed_in_seconds: number;
        start_checksession: boolean;
    };
    toastr: {
        timeout: number;
    };
    signalR: {
        url: string;
        retry: {
            timeout: number;
        }
    };
}
