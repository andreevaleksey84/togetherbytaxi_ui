import {HTTP_INTERCEPTORS} from '@angular/common/http';
import {ModuleWithProviders, NgModule} from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import {ApplicationNameInterceptor} from 'src/app/core/interceptors/applicationname.interceptor';
import {AuthInterceptor} from 'src/app/core/interceptors/auth.interceptor';
import {LanguageInterceptor} from 'src/app/core/interceptors/language.interceptor';
import {RequestIdInterceptor} from 'src/app/core/interceptors/requestid.interceptor';
import {AlertService} from 'src/app/core/services/alert.service';
import {AuthService} from 'src/app/core/services/auth.service';
import {PagingLocalStorageService} from 'src/app/core/services/paging-local-storage.service';
import {SignalrService} from 'src/app/core/services/signalr.service';
import {UserService} from 'src/app/core/services/user.service';
import { ChatService } from './services/chat.service';

@NgModule({
    providers: [
        {provide: HTTP_INTERCEPTORS, useClass: AuthInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: LanguageInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: RequestIdInterceptor, multi: true},
        {provide: HTTP_INTERCEPTORS, useClass: ApplicationNameInterceptor, multi: true},
    ],
})
export class CoreModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: CoreModule,
            providers: [
                TranslateService,
                AlertService,
                AuthService,
                PagingLocalStorageService,
                SignalrService,
                UserService,
                ChatService,
            ],
        };
    }
}
