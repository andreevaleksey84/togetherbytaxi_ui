export class ChatDirectMessageModel {
    public clientidfrom: string;
    public clientidto: string;
    public type: string;
    public message: string;
    public date: Date;    
}
