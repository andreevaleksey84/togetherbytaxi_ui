export class User {
  constructor(userId: string, userName: string, firstName?: string, lastName?: string, email?: string, emailVerified?: boolean, permissions?: Array<string>) {
    this.userId = userId;
    this.userName = userName;
    this.firstName = firstName;
    this.lastName = lastName;
    this.email = email;
    this.emailVerified = emailVerified;
    this.permissions = permissions;
  }

  public userId: string;
  public userName: string;
  public firstName: string;
  public lastName: string;
  public email: string;
  public emailVerified: boolean;
  public permissions: Array<string>;
}


