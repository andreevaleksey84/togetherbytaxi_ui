import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'truncate'
})

export class TruncatePipe implements PipeTransform {
    transform(name: string, textLength: number = 27) {
        if (name.length > textLength) { name = name.slice(0, textLength) + '...';
        }
        return name;
      }
}
