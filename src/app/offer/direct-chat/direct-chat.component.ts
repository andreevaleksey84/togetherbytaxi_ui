import {Component, OnInit, OnDestroy, NgZone, Input} from '@angular/core';
import {ITakeUntilDestroy, TakeUntilDestroy} from '../../core/services/destroySubjects.decorator';
import { ChatService } from 'src/app/core/services/chat.service';
import { AlertService } from 'src/app/core/services/alert.service';
import { ChatDirectMessageModel } from 'src/app/core/models/chat-direct-message-model';
import { AuthService } from 'src/app/core/services/auth.service';

@Component({
    selector: 'app-direct-chat',
    templateUrl: './direct-chat.component.html',
    styleUrls: ['./direct-chat.component.scss']
})

@TakeUntilDestroy
export class DirectChatComponent implements OnInit, OnDestroy, ITakeUntilDestroy {
    public componentDestroy: any;
    @Input() toUserId: string;
    @Input() buttonText: string;
    @Input() message: string;
    @Input() showMessageAboutNotReceived: boolean;
    private messageSent: boolean;

    constructor(
        private _chatService: ChatService,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _ngZone: NgZone,
        ) {
            this.messageSent = false;
            this.subscribeToChatEvents();
    }

    ngOnInit() {
    }

    ngOnDestroy() {
    }

    sendDirectMessage(): void {
        if(!this.toUserId) {
            console.error('Не задан пользователь, кому отправлять сообщение');
            return;
        }
        this.messageSent = false;
        this._chatService.sendDirectMessageWithText(this.message, this.toUserId);
        this.messageSent = true;
        if(this.showMessageAboutNotReceived){
            setTimeout(() => {
                if(this.messageSent) {
                    this._alertService.rideTogetherNotReceived();
                }
            }, 5000);
        }
    }

    private subscribeToChatEvents(): void {
        this._chatService.directMessageReceived.subscribe((message: ChatDirectMessageModel) => {
            this._ngZone.run(() => {
                if(this._authService.isAuthorized && message.clientidto == this._authService.currentUser.userId) {
                    if(message.message == 'ReceivedRideTogether') {
                        this.messageSent = false;
                    }
                }
            });
        });
    }
}
