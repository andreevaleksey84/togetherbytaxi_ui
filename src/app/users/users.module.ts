import {CommonModule} from '@angular/common';
import {NgModule} from '@angular/core';
import {TranslateModule} from '@ngx-translate/core';
import {TableModule} from 'primeng/table';
import {UserRoutingModule} from 'src/app/users/user-routing.module';
import {UserCardComponent} from './user-card/user-card.component';

@NgModule({
    imports: [
        CommonModule,
        TranslateModule,
        TableModule,
        UserRoutingModule
    ],
    declarations: [
        UserCardComponent
    ]
})

export class UsersModule {
}
