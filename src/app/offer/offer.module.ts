import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {GenerateComponent} from './generate/generate.component';
import {OfferRoutingModule} from './offer-routing.module';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {InputSwitchModule} from 'primeng/inputswitch';
import {SpeechRecognitionService} from '../core/models/speech-recognition-service';
import { AngularYandexMapsModule  } from 'angular8-yandex-maps';
import {AgmCoreModule} from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import {GeocodeGoogleService} from '../core/services/geocode.google.service';
import {GeocodeYandexService} from '../core/services/getcode.yandex.service';
import {MyOffersComponent} from './my-offers/my-offers.component';
import {FoundLocationComponent} from './found-location/found-location.component';
import {YesNoLabelComponent} from './yes-no-label/yes-no-label.component';
import { DirectChatComponent } from './direct-chat/direct-chat.component';
import { FindLocationComponent } from './find-location/find-location.component';
import { NgbAccordionModule } from '@ng-bootstrap/ng-bootstrap';

@NgModule({
    declarations: [GenerateComponent, MyOffersComponent, FoundLocationComponent, YesNoLabelComponent, DirectChatComponent, FindLocationComponent ],
    imports: [
        CommonModule,
        OfferRoutingModule,
        FormsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBbijWc72ZbIXnnBMbyzfLPA-0M50_7duk',
            libraries: ['places']
        }),
        AgmDirectionModule,
        TranslateModule,
        InputSwitchModule,
        AngularYandexMapsModule.forRoot({
            apikey: '48b2a0a7-5366-44e5-a386-3906e470fef8',
            lang: 'ru_RU',
        }),
        NgbAccordionModule,
    ],
    providers: [
        GeocodeGoogleService,
        GeocodeYandexService,
        SpeechRecognitionService,
    ]
})
export class OfferModule {
}
