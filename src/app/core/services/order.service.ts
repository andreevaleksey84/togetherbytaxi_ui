import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppConfig} from 'src/app/app.config';
import {CreateOrderModel} from '../models/create-order-model';
import {OrderInfoModel} from '../models/order-info-model';

@Injectable({
    providedIn: 'root'
})
export class OrderService {

    private requestHeader: HttpHeaders;
    private readonly baseUrl = `${AppConfig.settings.orderServiceUrl}/api/v1/order`;

    constructor(private _httpClient: HttpClient) {
        this.requestHeader = new HttpHeaders();
        this.requestHeader.append('Content-Type', 'application/json');
    }

    /**
     * Сформировать заказ
     * @param createOrderModel
     */
    public create(createOrderModel: CreateOrderModel): Observable<any> {
        return this._httpClient.post<CreateOrderModel>(`${this.baseUrl}`, createOrderModel);
    }

    /**
     * Получить все заказы пользователя
     */
    public getAll(): Observable<OrderInfoModel[]> {
        return this._httpClient.get<OrderInfoModel[]>(`${this.baseUrl}/all`);
    }
}
