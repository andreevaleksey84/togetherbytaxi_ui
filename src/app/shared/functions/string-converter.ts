/**
 * Конвертер строк
 */
export class StringConverter {
    /**
     * String to Base64
     * @param string - Строка
     */
    static convertStringToBase64(string: string): string {
        return window.btoa(unescape(encodeURIComponent(string)));
    }

    /**
     * Base64 to String
     * @param string - Строка в Base64
     */
    static convertBase64ToString(string: string): string {
        return decodeURIComponent(escape(window.atob(string)));
    }
}
