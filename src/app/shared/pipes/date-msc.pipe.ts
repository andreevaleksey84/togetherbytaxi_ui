import {Pipe, PipeTransform} from '@angular/core';
import {formatDate} from '@angular/common';
import * as moment from 'moment';

@Pipe({
    name: 'datemsc'
})

export class DateMscPipe implements PipeTransform {
    transform(value: any, format: string, timezone?: string, locale?: string) {
        if (value == null || value === '') {
            return null;
        }

        const momentDate = moment(value).toString();

        if (timezone == null || timezone === '') {
            timezone = '+0300';
        }

        if (locale == null || locale === '') {
            const language = window.navigator.language || 'ru-RU';
            locale = moment.locale(language);
        }

        return formatDate(momentDate, format, locale, timezone);
    }
}
