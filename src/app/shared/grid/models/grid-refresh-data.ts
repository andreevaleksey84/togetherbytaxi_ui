import {IFilter} from 'src/app/shared/interfaces/filter.interface';
import {RefreshDataType} from 'src/app/shared/enums/refresh-data.type.enum';

export class GridRefreshData {

  filter: IFilter;

  dataFlowType: RefreshDataType = RefreshDataType.Default;

  constructor(filter: IFilter, dataFlowType: RefreshDataType) {
    this.filter = filter;
    this.dataFlowType = dataFlowType;
  }
}
