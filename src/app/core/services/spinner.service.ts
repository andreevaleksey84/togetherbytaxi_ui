import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SpinnerService {
    isSpinnerVisible$ = new BehaviorSubject(false);
    constructor() {}

    show() {
        this.isSpinnerVisible$.next(true);
    }

    hide() {
        this.isSpinnerVisible$.next(false);
    }

}
