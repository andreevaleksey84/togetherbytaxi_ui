import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListComponent} from './list/list.component';
import {TaxiRoutingModule} from './taxi-routing.module';

@NgModule({
    declarations: [ListComponent],
    imports: [
        CommonModule,
        TaxiRoutingModule
    ]
})
export class TaxiModule {
}
