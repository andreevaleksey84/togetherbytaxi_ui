import {registerLocaleData} from '@angular/common';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import localeRu from '@angular/common/locales/ru';
import {APP_INITIALIZER, LOCALE_ID, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {TranslateLoader, TranslateModule, TranslateService} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {AuthModule, AuthWellKnownEndpoints, OidcSecurityService, OpenIDImplicitFlowConfiguration} from 'angular-auth-oidc-client';
import {ToastrModule} from 'ngx-toastr';
import {CoreModule} from 'src/app/core/core.module';
import {environment} from '../environments/environment';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {AppConfig} from './app.config';
import {Constants} from './core/consts/constants';
import {SharedModule} from './shared/shared.module';
import { CurrencyMaskModule } from 'ng2-currency-mask';
import { CURRENCY_MASK_CONFIG } from 'ng2-currency-mask/src/currency-mask.config';
import { CustomCurrencyMaskConfig } from './shared/constants/custom-currency-mask-config';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import {YesNoLabelComponent} from './offer/yes-no-label/yes-no-label.component';

registerLocaleData(localeRu, 'ru');

export function initializeApp(appConfig: AppConfig) {
    return () => appConfig.load();
}

// AoT requires an exported function for factories
export function HttpLoaderFactory(httpClient: HttpClient) {
    return new TranslateHttpLoader(httpClient);
}

@NgModule({
    declarations: [
        AppComponent,
    ],
    imports: [
        BrowserAnimationsModule,
        HttpClientModule,
        BrowserModule,
        CoreModule.forRoot(),
        AuthModule.forRoot(),
        AppRoutingModule,
        SharedModule,
        TranslateModule.forRoot({
            loader: {
                provide: TranslateLoader,
                useFactory: HttpLoaderFactory,
                deps: [HttpClient]
            }
        }),
        ToastrModule.forRoot({
            timeOut: environment.toastr.timeout,
            positionClass: 'toast-bottom-right',
            preventDuplicates: true
        }),
        CurrencyMaskModule,
        ProgressSpinnerModule
    ],
    providers: [
        AppConfig,
        {provide: APP_INITIALIZER, useFactory: initializeApp, deps: [AppConfig], multi: true},
        {provide: LOCALE_ID, useValue: 'ru'},
        Constants,
        { provide: CURRENCY_MASK_CONFIG, useValue: CustomCurrencyMaskConfig }
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
    constructor(
        private oidcSecurityService: OidcSecurityService,
        private constants: Constants,
        private appConfig: AppConfig,
        private translateService: TranslateService
    ) {
        const langCodes = constants.SupportedLanguages.map(function(val, index) {
            return val.code;
        });
        // initialize translate service
        this.translateService.addLangs(langCodes);
        this.translateService.setDefaultLang(constants.DefaultLanguage);
        appConfig.load().then(e => {
            const openIDImplicitFlowConfiguration = new OpenIDImplicitFlowConfiguration();
            openIDImplicitFlowConfiguration.stsServer = AppConfig.settings.openIDImplicitFlowConfiguration.issuer;
            openIDImplicitFlowConfiguration.redirect_url = AppConfig.settings.openIDImplicitFlowConfiguration.redirect_url;
            openIDImplicitFlowConfiguration.client_id = AppConfig.settings.openIDImplicitFlowConfiguration.client_id;
            openIDImplicitFlowConfiguration.response_type = AppConfig.settings.openIDImplicitFlowConfiguration.response_type;
            openIDImplicitFlowConfiguration.scope = AppConfig.settings.openIDImplicitFlowConfiguration.scope;
            openIDImplicitFlowConfiguration.post_logout_redirect_uri = AppConfig.settings.openIDImplicitFlowConfiguration.post_logout_redirect_uri;
            openIDImplicitFlowConfiguration.post_login_route = AppConfig.settings.openIDImplicitFlowConfiguration.post_login_route;
            openIDImplicitFlowConfiguration.trigger_authorization_result_event = AppConfig.settings.openIDImplicitFlowConfiguration.trigger_authorization_result_event;
            openIDImplicitFlowConfiguration.silent_renew = AppConfig.settings.openIDImplicitFlowConfiguration.silent_renew;
            openIDImplicitFlowConfiguration.silent_renew_url = AppConfig.settings.openIDImplicitFlowConfiguration.silent_renew_url;
            openIDImplicitFlowConfiguration.log_console_warning_active = AppConfig.settings.openIDImplicitFlowConfiguration.log_console_warning_active;
            openIDImplicitFlowConfiguration.log_console_debug_active = AppConfig.settings.openIDImplicitFlowConfiguration.log_console_debug_active;
            openIDImplicitFlowConfiguration.forbidden_route = '/';
            openIDImplicitFlowConfiguration.unauthorized_route = '/';
            openIDImplicitFlowConfiguration.max_id_token_iat_offset_allowed_in_seconds = AppConfig.settings.openIDImplicitFlowConfiguration.max_id_token_iat_offset_allowed_in_seconds;
            openIDImplicitFlowConfiguration.start_checksession = AppConfig.settings.openIDImplicitFlowConfiguration.start_checksession;
            openIDImplicitFlowConfiguration.storage = localStorage;

            const authWellKnownEndpoints = new AuthWellKnownEndpoints();
            authWellKnownEndpoints.issuer = AppConfig.settings.openIDImplicitFlowConfiguration.issuer;
            authWellKnownEndpoints.jwks_uri = AppConfig.settings.openIDImplicitFlowConfiguration.issuer + '/.well-known/openid-configuration/jwks';
            authWellKnownEndpoints.authorization_endpoint = AppConfig.settings.openIDImplicitFlowConfiguration.issuer + '/connect/authorize';
            authWellKnownEndpoints.token_endpoint = AppConfig.settings.openIDImplicitFlowConfiguration.issuer + '/connect/token';
            authWellKnownEndpoints.userinfo_endpoint = AppConfig.settings.openIDImplicitFlowConfiguration.issuer + '/connect/userinfo';
            authWellKnownEndpoints.end_session_endpoint = AppConfig.settings.openIDImplicitFlowConfiguration.issuer + '/connect/endsession';
            authWellKnownEndpoints.check_session_iframe = AppConfig.settings.openIDImplicitFlowConfiguration.issuer + '/connect/checksession';
            authWellKnownEndpoints.revocation_endpoint = AppConfig.settings.openIDImplicitFlowConfiguration.issuer + '/connect/revocation';
            authWellKnownEndpoints.introspection_endpoint = AppConfig.settings.openIDImplicitFlowConfiguration.issuer + '/connect/introspect';

            this.oidcSecurityService.setupModule(openIDImplicitFlowConfiguration, authWellKnownEndpoints);

        });
    }
}
