import {Component, EventEmitter, Input, OnChanges, OnInit, Output} from '@angular/core';

@Component({
    selector: 'app-date-time-picker',
    templateUrl: './date-time-picker.component.html',
    styleUrls: ['./date-time-picker.component.css']
})
export class DateTimePickerComponent {
    @Input() value: Date;
    @Input() isDisabled: boolean;
    @Input() label: string;
    @Input() placeholder: string;
    @Output() valueChange: EventEmitter<any> = new EventEmitter<any>();

    // public dateTimeFormat = 'DD/MM/YYYY HH:mm';
    // public dateTimeLocale;
    // public valueFromInput;

    constructor(
        // private _translateService: TranslateService,
        // private constants: Constants
    ) {
    }

    onChange = (text: string) => this.valueChange.emit(text);

    // ngOnInit() {
        // let lang = this._translateService.currentLang === ''
        //     ? this._translateService.defaultLang
        //     : this._translateService.currentLang;
        // this.dateTimeLocale = this.constants.LanguagesForDateTimePicker[lang];
        // this._translateService.onLangChange.subscribe(() => {
        //     lang = this._translateService.currentLang;
        //     this.dateTimeLocale = this.constants.LanguagesForDateTimePicker[lang];
        // });
    // }

    // ngOnChanges(changes) {
    //     if (!isNullOrUndefined(changes.value)) {
    //         if (this.value) {
    //             this.value = new Date(this.value);
    //         }
    //     }
    // }

    // public onInputValue(event) {
    //     if (event && event.target && event.target.value) {
    //         this.formatInputValueToDate(event.target.value);
    //     }
    // }

    // formatInputValueToDate(value) {
    //     if (value.length === this.dateTimeFormat.length) {
    //         this.resetValueFromInput();
    //         if (value.indexOf(':') !== 13) {
    //             return;
    //         }
    //         if (Date.parse(value)) {
    //             return this.valueChange.emit(new Date(value));
    //         }
    //     }
    //     if (value.length > this.dateTimeFormat.length) {
    //         this.resetValueFromInput();
    //         return;
    //     }
    //     if (Date.parse(value)) {
    //         this.valueFromInput = value;
    //     }
    // }

    // public onSelectValue(value) {
    //     this.valueChange.emit(value);
    // }

    // resetValueFromInput() {
    //     if (this.valueFromInput) {
    //         this.valueFromInput = null;
    //     }
    // }

    // getPlaceHolder() {
    //     if (this.valueFromInput) {
    //         return this.getCurrentDate(this.valueFromInput);
    //     }
    //     return this.value ? this.getCurrentDate(this.value) : this.placeholder;
    // }

    // checkIsDisabled() {
    //     return this.isDisabled;
    // }

    // getCurrentDate(value) {
    //     return moment(value).format(this.dateTimeFormat);
    // }
}
