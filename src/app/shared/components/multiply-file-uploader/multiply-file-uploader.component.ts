import {Component, EventEmitter, Input, OnInit, Output, ViewChild} from '@angular/core';
import {FileService} from 'src/app/core/services/file.service';
import {AlertService} from '../../../core/services/alert.service';
import {FileAsData} from '../../models/file-as-data';

@Component({
  selector: 'app-multiply-file-uploader',
  templateUrl: './multiply-file-uploader.component.html',
  styleUrls: ['./multiply-file-uploader.component.css']
})
export class MultiplyFileUploaderComponent implements OnInit {
  @Input() choiceLabel = 'Deal.PaperDeal.Upload';
  @Input() formats = '*/*';
  @Output() fileIds: EventEmitter<any> = new EventEmitter<any>();
  @Output() filesWithHashes: EventEmitter<Array<FileAsData>> = new EventEmitter<Array<FileAsData>>();
  @Input() showLoading = true;
  @Input() showNames = true;
  @Input() clearAfterUpload = false;
  @Input() multiple = true;
  @ViewChild('uploader') uploader;

  formData: FormData = new FormData();
  fileNames = [];
  isMultiplyUploading = false;
  fileIsUploading = 'Идет загрузка';
  dotsProgressBar = '';

  constructor(private _fileService: FileService,
              private _alertService: AlertService
  ) {
  }

  ngOnInit() {
  }

  /**
   * Загрузка нескольких файлов
   * @param evt
   */
  onSelectFiles(evt: any) {
    this.startMultiplyProgressBar();
    const fileNames = [];
    for (let i = 0; i < evt.files.length; i++) {
      this.formData.append(evt.files[i].name, evt.files[i]);
      fileNames.push(evt.files[i].name);
    }

    this._fileService.post(this.formData).subscribe(success => {
        if (success) {
          this.fileNames = fileNames;
          this.isMultiplyUploading = false;
          this.filesWithHashes.emit(success);
          this.fileIds.emit(success.map(s => s.id));
          if (this.clearAfterUpload) {
              this.uploader.clear();
              this.formData = new FormData();
          }
        }
      },
      error => {
        this.OnErrorResult(error.error);
        this.isMultiplyUploading = false;
      });
  }

  startInterval() {
    const interval = setInterval(() => {
      if (this.dotsProgressBar.length > 3) {
        this.dotsProgressBar = '';
      } else {
        this.dotsProgressBar += '.';
      }
      if (!this.isMultiplyUploading) {
        clearInterval(interval);
      }
    }, 150);
  }

  startMultiplyProgressBar() {
    this.isMultiplyUploading = true;
    this.startInterval();
  }

  getFormats(): string {
    if (this.formats) {
      return this.formats;
    }
  }

  OnErrorResult(message: string) {
    this._alertService.error(message);
  }
}
