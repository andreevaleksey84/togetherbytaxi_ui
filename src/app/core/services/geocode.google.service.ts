import {Injectable, NgZone, ElementRef} from '@angular/core';
import {MapsAPILoader} from '@agm/core';
import {tap, map, switchMap} from 'rxjs/operators';
import {ILocation} from '../models/ilocation';
import {Observable, of} from 'rxjs';
import {fromPromise} from 'rxjs/internal-compatibility';

declare var google: any;

@Injectable({
    providedIn: 'root'
})
export class GeocodeGoogleService {
    private geocoder: any;

    constructor(private _mapLoader: MapsAPILoader,
                private ngZone: NgZone
    ) {
    }

    static createLocationObject(lat: number, lng: number) {
        return {lat: lat, lng: lng};
    }

    private initGeocoder() {
        console.log('Init geocoder!');
        this.geocoder = new google.maps.Geocoder();
    }

    private waitForMapsToLoad(): Observable<boolean> {
        if (!this.geocoder) {
            return fromPromise(this._mapLoader.load())
                .pipe(
                    tap(() => this.initGeocoder()),
                    map(() => true)
                );
        }
        return of(true);
    }

    geocodeAddress(address: string): Observable<ILocation> {
        console.log('Start geocode by address!');
        // @ts-ignore
        return this.waitForMapsToLoad().pipe(
            // filter(loaded => loaded),
            switchMap(() => {
                return new Observable(observer => {
                    this.geocoder.geocode({'address': address}, (results, status) => {
                        if (status === google.maps.GeocoderStatus.OK) {
                            console.log('Geocoding complete!');
                            observer.next({
                                lat: results[0].geometry.location.lat(),
                                lng: results[0].geometry.location.lng()
                            });
                        } else {
                            console.log('Error - ', results, ' & Status - ', status);
                            observer.next({lat: 0, lng: 0});
                        }
                        observer.complete();
                    });
                });
            })
        );
    }

    geocode(location: ILocation): Observable<string> {
        console.log('Start geocode by location!');
        // @ts-ignore
        return this.waitForMapsToLoad().pipe(
            // filter(loaded => loaded),
            switchMap(() => {
                return new Observable(observer => {
                    this.geocoder.geocode({'location': location}, (results, status) => {
                        if (status === google.maps.GeocoderStatus.OK) {
                            console.log('Geocoding complete!');
                            observer.next(results[0].formatted_address);
                        } else {
                            console.log('Error - ', results, ' & Status - ', status);
                            observer.next('');
                        }
                        observer.complete();
                    });
                });
            })
        );
    }

    currentLocation(): Observable<ILocation> {
        console.log('Start search current location!');
        // @ts-ignore
        return this.waitForMapsToLoad().pipe(
            // filter(loaded => loaded),
            switchMap(() => {
                return new Observable(observer => {
                    if ('geolocation' in navigator) {
                        navigator.geolocation.getCurrentPosition((position) => {
                            observer.next({
                                lat: position.coords.latitude,
                                lng: position.coords.longitude
                            });
                        });
                    }
                });
            })
        );
    }

    addListenerAutoComplete(element: ElementRef): Observable<ILocation> {
        console.log('Start addListener AutoComplete!');
        // @ts-ignore
        return this.waitForMapsToLoad().pipe(
            // filter(loaded => loaded),
            switchMap(() => {
                return new Observable(observer => {
                    if (element) {
                        const autocompleteElement = new google.maps.places.Autocomplete(element.nativeElement, {
                            types: ['address']
                        });
                        autocompleteElement.addListener('place_changed', () => {
                            this.ngZone.run(() => {
                                // get the place result
                                const place: google.maps.places.PlaceResult = autocompleteElement.getPlace();
                                // verify result
                                if (place.geometry === undefined || place.geometry === null) {
                                    return;
                                }
                                observer.next({
                                    lat: place.geometry.location.lat(),
                                    lng: place.geometry.location.lng()
                                });
                            });
                        });
                    } else {
                        console.log('Element for autocomplete is not defined');
                    }
                });
            })
        );
    }
}
