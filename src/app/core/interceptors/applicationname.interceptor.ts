import {HttpEvent, HttpHandler, HttpInterceptor, HttpRequest} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ApplicationNameInterceptor implements HttpInterceptor {

  constructor() {
  }

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    const requestToForward = req.clone({setHeaders: {'X-TogetherByTaxi-ApplicationInitiatorName': 'TogetherByTaxi'}});
    return next.handle(requestToForward);
  }
}
