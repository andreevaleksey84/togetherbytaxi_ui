import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {MainComponent} from './main/main.component';
import {InfoRoutingModule} from './info-routing.module';
import {AgmCoreModule} from '@agm/core';
import { AgmDirectionModule } from 'agm-direction';
import {FormsModule} from '@angular/forms';
import {TranslateModule} from '@ngx-translate/core';
import {InputSwitchModule} from 'primeng/inputswitch';
import {SpeechRecognitionService} from '../core/models/speech-recognition-service';
import {AngularSvgIconModule} from 'angular-svg-icon';
import {GeocodeGoogleService} from '../core/services/geocode.google.service';
import {GeocodeYandexService} from '../core/services/getcode.yandex.service';

@NgModule({
    declarations: [MainComponent],
    imports: [
        CommonModule,
        InfoRoutingModule,
        FormsModule,
        AgmCoreModule.forRoot({
            apiKey: 'AIzaSyBbijWc72ZbIXnnBMbyzfLPA-0M50_7duk',
            libraries: ['places']
        }),
        AgmDirectionModule,
        TranslateModule,
        InputSwitchModule,
        AngularSvgIconModule,
    ],
    providers: [
        GeocodeYandexService,
        GeocodeGoogleService,
        SpeechRecognitionService,
    ]
})
export class InfoModule {
}

