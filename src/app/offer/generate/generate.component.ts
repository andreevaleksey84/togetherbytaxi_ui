import {Component, OnInit, OnDestroy, ViewChild, ElementRef, AfterViewInit, NgZone} from '@angular/core';
import {ILocation} from '../../core/models/ilocation';
import {PreOrderService} from 'src/app/core/services/preorder.service';
import {PreOrderCreateModel} from 'src/app/core/models/preorder-create-model';
import {AlertService} from 'src/app/core/services/alert.service';
import {AuthService} from 'src/app/core/services/auth.service';
import {PreOrderChangeModel} from 'src/app/core/models/preorder-change-model';
import {LocationService} from 'src/app/core/services/location.service';
import {TranslateService} from '@ngx-translate/core';
import {CreateOrderModel} from '../../core/models/create-order-model';
import {OrderService} from '../../core/services/order.service';
import {SpeechRecognitionService} from '../../core/models/speech-recognition-service';
import {ITakeUntilDestroy, TakeUntilDestroy} from '../../core/services/destroySubjects.decorator';
import {takeUntil} from 'rxjs/operators';
import {GeocodeGoogleService} from '../../core/services/geocode.google.service';
import {GeocodeYandexService} from '../../core/services/getcode.yandex.service';
import {FindNearbyLocationResponse} from '../../core/models/find-nearby-location-response';
import { ChatDirectMessageModel } from 'src/app/core/models/chat-direct-message-model';
import { ChatService } from 'src/app/core/services/chat.service';
declare var $: any;
declare var ymaps: any;

@Component({
    selector: 'app-generate',
    templateUrl: './generate.component.html',
    styleUrls: ['./generate.component.scss']
})

@TakeUntilDestroy
export class GenerateComponent implements OnInit, OnDestroy, ITakeUntilDestroy, AfterViewInit {
    public componentDestroy: any;
    addressFrom: string;
    addressTo: string;
    locationFrom: ILocation;
    locationTo: ILocation;
    loading: boolean;
    currentPreOrderUniqId: any;

    zoom: number;

    state = {
        preOrder: true,
        changePreOrder: false,
        createOrder: false,
        orderCreated: false
    };

    speechRecognitionEnabled: boolean;
    speechRecognitionAddressFromPressed: boolean;

    @ViewChild('searchFrom') public searchFromElementRef: ElementRef;
    @ViewChild('searchTo') public searchToElementRef: ElementRef;

    constructor(
        private _geocodeGoogleService: GeocodeGoogleService,
        private _geocodeYandexService: GeocodeYandexService,
        private _preOrderService: PreOrderService,
        private _alertService: AlertService,
        private _authService: AuthService,
        private _locationService: LocationService,
        private _translateService: TranslateService,
        private _orderService: OrderService,
        private _speechRecognitionService: SpeechRecognitionService,
    ) {
        this.speechRecognitionAddressFromPressed = false;
    }

    ngOnInit() {
        if (!('webkitSpeechRecognition' in window)) {
            this.speechRecognitionEnabled = false;
            console.log('Speech recognition is FAILED');
        } else {
            //  this.initSpeechInputs();
            this.speechRecognitionEnabled = true;
            this._translateService.onLangChange
                .pipe(takeUntil(this.componentDestroy()))
                .subscribe(() => {
                    this._speechRecognitionService.setLanguage();
                });
            console.log('Speech recognition is OK');
        }
        this.setDefault();
        this.setCurrentLocation();
        setTimeout(() => {
            // this.addAutoCompleteForAddressFrom();
            // this.addAutoCompleteForAddressTo();
        }, 500);
    }

    ngOnDestroy() {
        this._speechRecognitionService.DestroySpeechObject();
    }

    ngAfterViewInit() {
        const that = this;
        $('#addressFrom')
            .suggestions({
                token: '14fb83fe0765736034c286f90590acd06a464c99',
                type: 'ADDRESS',
                /* Вызывается, когда пользователь выбирает одну из подсказок */
                onSelect: function(suggestion) {
                    that.addressFrom = $('#addressFrom').val();
                    that.locationFrom = { lat: suggestion.data.geo_lat, lng: suggestion.data.geo_lon };
                }
            });

        $('#addressTo')
            .suggestions({
                token: '14fb83fe0765736034c286f90590acd06a464c99',
                type: 'ADDRESS',
                /* Вызывается, когда пользователь выбирает одну из подсказок */
                onSelect: function(suggestion) {
                    that.addressTo = $('#addressTo').val();
                    that.locationTo = { lat: suggestion.data.geo_lat, lng: suggestion.data.geo_lon };
                }
            });
    }

    get classForSpeechRecognitionAddressFrom() {
        return this.speechRecognitionAddressFromPressed ? 'mic-button-pressed' : 'mic-button';
    }

    async startStopSpeechSearchMovie() {
        this.speechRecognitionAddressFromPressed = !this.speechRecognitionAddressFromPressed;

        if (this.speechRecognitionAddressFromPressed) {
            this._speechRecognitionService.record()
                .pipe(takeUntil(this.componentDestroy()))
                .subscribe(
                    // listener
                    async (value) => {
                        console.log(value);
                        // TODO: заменить на сервис дадата
                        const loc = await this._geocodeGoogleService.geocodeAddress(value).toPromise();
                        this.setLocationFrom(loc);
                    },
                    // error
                    (err) => {
                        console.log(err);
                        if (err.error === 'no-speech') {
                            console.log('--restarting service--');
                            this.speechRecognitionAddressFromPressed = false;
                        }
                    },
                    // completion
                    () => {
                        console.log('--complete--');
                        this.speechRecognitionAddressFromPressed = false;
                    });
        } else {
            this._speechRecognitionService.stop();
        }
    }

    isAuthorized() {
        return this._authService.isAuthorized;
    }

    startNew() {
        this.setDefault();
    }

    goToChangePreOrderState() {
        this.state.preOrder = false;
        this.state.changePreOrder = true;
        this.state.createOrder = false;
        this.state.orderCreated = false;
    }

    async changePreOrder() {
        if (!this.checkBeforeChangePreOrder()) {
            return;
        }
        try {
            const preOrder = this.generateModelToChangePreOrder();
            const res = await this._preOrderService.change(preOrder).toPromise();
            const messageChangeSuccess = this._translateService.instant('GenerateOffer.Messages.EditSuccess');
            this._alertService.success(messageChangeSuccess);
            this.currentPreOrderUniqId = res;
            this.state.preOrder = false;
            this.state.changePreOrder = false;
            this.state.createOrder = true;
            this.state.orderCreated = false;
        } catch (error) {
            console.error('Error during send request to create preOrder', error);
            this.showErrorOccurs();
        }
    }

    async createOrder() {
        try {
            const order = this.generateModelToCreateOrder();
            await this._orderService.create(order).toPromise();
            const messageCreateOrderSuccess = this._translateService.instant('GenerateOffer.Messages.CreateOrderSuccess');
            this._alertService.success(messageCreateOrderSuccess);
            this.state.preOrder = false;
            this.state.changePreOrder = false;
            this.state.createOrder = false;
            this.state.orderCreated = true;
        } catch (error) {
            console.error('Error during send request to create preOrder', error);
            this.showErrorOccurs();
        }
    }

    private checkBeforeChangePreOrder(): boolean {
        if (!this.checkBeforeCreatePreOrder()) {
            return false;
        }
        if (!this.currentPreOrderUniqId) {
            this._alertService.warning('Не выбран заказ, который вы хотите поменять. Пожалуйста, начните процесс оформления заново');
            return false;
        }
        return true;
    }

    private generateModelToCreateOrder(): CreateOrderModel {
        const createOrder = new CreateOrderModel();
        createOrder.preOrderUniqId = this.currentPreOrderUniqId;
        return createOrder;
    }

    private generateModelToChangePreOrder(): PreOrderChangeModel {
        let preOrder = new PreOrderChangeModel();
        preOrder = this.setModelToCreatePreOrder(preOrder);
        preOrder.preOrderChangeId = this.currentPreOrderUniqId;
        return preOrder;
    }

    private setDefault() {
        this.state.preOrder = true;
        this.state.changePreOrder = false;
        this.state.createOrder = false;
        this.currentPreOrderUniqId = undefined;
    }

    // Get Current Location Coordinates
    private setCurrentLocation() {
        ymaps.geolocation
            .get({
                provider: 'yandex',
                mapStateAutoApply: true,
            })
            .then((result) => {
                const currentLocation = {
                    lat: result.geoObjects.position[0],
                        lng: result.geoObjects.position[1]
                };
                this.setLocationFrom(currentLocation);
            });

        /*
        this._geocodeYandexService.currentLocation()
            .pipe(takeUntil(this.componentDestroy()))
            .subscribe((currentLocation: ILocation) => {
                    this.setLocationFrom(currentLocation);
                }
            );

         */
    }

    private setLocationFrom(location: ILocation) {
        this.locationFrom = location;
        this.zoom = 8;
        this.loading = false;
    }

    async createPreOrder() {
        if (!this.checkBeforeCreatePreOrder()) {
            return;
        }
        try {
            const preOrder = this.generateModelToCreatePreOrder();
            const res = await this._preOrderService.create(preOrder).toPromise();
            this._alertService.success('Оформление начато успешно. Вы можете продолжить');
            console.log('Current PreOrder unique id', res);
            this.currentPreOrderUniqId = res;
            this.state.preOrder = false;
            this.state.changePreOrder = false;
            this.state.createOrder = true;
        } catch (error) {
            console.log('Error during send request to create preOrder', error);
            this.showErrorOccurs();
        }
    }

    private generateModelToCreatePreOrder(): PreOrderCreateModel {
        let preOrder = new PreOrderCreateModel();
        preOrder = this.setModelToCreatePreOrder(preOrder);
        return preOrder;
    }

    private setModelToCreatePreOrder(preOrder) {
        preOrder.locationFromLat = this.locationFrom.lat;
        preOrder.locationFromLng = this.locationFrom.lng;
        preOrder.locationFromAddress = this.addressFrom;

        preOrder.locationToLat = this.locationTo.lat;
        preOrder.locationToLng = this.locationTo.lng;
        preOrder.locationToAddress = this.addressTo;
        return preOrder;
    }

    private checkBeforeCreatePreOrder(): boolean {
        if (!this.locationFrom || !this.locationFrom.lat || !this.locationFrom.lng || !this.addressFrom) {
            this._alertService.warning('Пожалуйста, заполните значение ОТКУДА вы хотите ехать');
            return false;
        }
        if (!this.locationTo || !this.locationTo.lat || !this.locationTo.lng || !this.addressTo) {
            this._alertService.warning('Пожалуйста, заполните значение КУДА вы хотите ехать');
            return false;
        }
        return true;
    }

    currentStateDescription(): string {
        if (this.state.preOrder) {
            return this._translateService.instant('GenerateOffer.Titles.TravelArrangement');
        }
        if (this.state.changePreOrder) {
            return this._translateService.instant('GenerateOffer.Titles.ChangeData');
        }
        if (this.state.createOrder) {
            return this._translateService.instant('GenerateOffer.Titles.StartSuccessPleaseContinue');
        }
    }

    private showErrorOccurs() {
        const errorMessage = this._translateService.instant('GenerateOffer.Messages.SorryError');
        this._alertService.error(errorMessage);
    }
}
