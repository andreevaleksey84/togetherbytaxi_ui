import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppConfig} from 'src/app/app.config';
import {TaxiListModel} from '../models/taxi-list-model';

@Injectable({
    providedIn: 'root'
})
export class TaxiListService {

    private requestHeader: HttpHeaders;
    private readonly baseUrl = `${AppConfig.settings.orderServiceUrl}/api/v1/taxi-list`;

    constructor(private _httpClient: HttpClient) {
        this.requestHeader = new HttpHeaders();
        this.requestHeader.append('Content-Type', 'application/json');
    }

    /**
     * Получить список всех такси
     */
    public getAll(): Observable<TaxiListModel[]> {
        return this._httpClient.get<TaxiListModel[]>(`${this.baseUrl}`);
    }
}
