export class ContactPersonInfo {
    public fullName: string;
    public phoneNumber: string;
    public email: string;
}
