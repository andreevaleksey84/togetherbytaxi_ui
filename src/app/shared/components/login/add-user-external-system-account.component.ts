import {Component, OnInit} from '@angular/core';

import {UserService} from 'src/app/core/services/user.service';
import {AppConfig} from '../../../app.config';

@Component({
  template: '<p>{{ "Login.Redirect" | translate }}</p>'
})

export class AddUserExternalSystemAccountComponent implements OnInit {

  private returnUrl = `${AppConfig.settings.openIDImplicitFlowConfiguration.post_login_route}`;
  private requestUrl = `${AppConfig.settings.openIDImplicitFlowConfiguration.issuer}`;

  constructor(private _userService: UserService) {
    this._userService.addExternalSystem(this.requestUrl, this.returnUrl);
  }

  ngOnInit() {

  }
}
