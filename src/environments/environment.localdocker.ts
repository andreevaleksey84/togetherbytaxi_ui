// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---deploy` replaces `environment.ts` with `environment.deploy.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  name: 'localdocker',
  production: false,
  toastr: {
    timeout: 3000
  }
};
