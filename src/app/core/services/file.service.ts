import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {AppConfig} from 'src/app/app.config';
import {FileAsData} from 'src/app/shared/models/file-as-data';

@Injectable({
    providedIn: 'root'
})
export class FileService {

    private requestHeader: HttpHeaders;
    private readonly baseUrl = `${AppConfig.settings.fileServiceUrl}/api/v1/files`;

    constructor(private _httpClient: HttpClient) {
        this.requestHeader = new HttpHeaders();
        this.requestHeader.append('Content-Type', 'application/json');
    }

    /**
     * Сохранить файл
     * @param formData
     */
    public post(formData: FormData): Observable<Array<FileAsData>> {
        return this._httpClient.post<Array<FileAsData>>(`${this.baseUrl}`, formData);
    }

    /**
     * Сохранить файлы
     * @param formData
     */
    public postFiles(formData: FormData): Observable<Array<FileAsData>> {
        return this._httpClient.post<Array<FileAsData>>(`${this.baseUrl}/UploadFiles`, formData);
    }

    public getUrlBy(fileId: string): string {
        return this.baseUrl + '/' + fileId + '/docx';
    }

    public getPreSaveUrlBy(fileId: string): string {
        return this.baseUrl + '/' + fileId + '/getpresavefile';
    }

    public getPreSaveFileNameBy(fileId: string): Observable<{ id: string, name: string }> {
        return this._httpClient.get<{ id: string, name: string }>(`${this.baseUrl}/${fileId}/presavefilename`);
    }

    public getPdfUrlBy(fileId: string): string {
        return this.baseUrl + '/' + fileId + '/pdf';
    }

    public getFileById(fileId: string): string {
        return this.baseUrl + '/' + fileId;
    }

    public getFileHash(fileId: string): Observable<FileAsData> {
        return this._httpClient.get<FileAsData>(`${this.baseUrl}/${fileId}/getFileHash`);
    }

    /**
     * Получение имени файла по его ИД
     * @param fileId - ИД файла
     */
    public getFileNameById(fileId: string): Observable<any> {
        return this._httpClient.get(`${this.baseUrl}/${fileId}/getfilename`);
    }
}
