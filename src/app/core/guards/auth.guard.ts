import {Injectable} from '@angular/core';
import {CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router} from '@angular/router';
import {Observable} from 'rxjs';
import {map} from 'rxjs/operators';

import {AuthService} from 'src/app/core/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {

  constructor(private _router: Router,
              private _authService: AuthService) {
  }

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> | boolean {
    // console.log(route + '' + state);
    // console.log('AuthGuardService, canActivate');

    return this._authService.getIsAuthorized().pipe(
      map((isAuthorized: boolean) => {
        // console.log('AuthGuardService, canActivate isAuthorized: ' + isAuthorized);

        if (isAuthorized) {
          return true;
        }
        localStorage.setItem('backUrlAfterLogin', state.url);
        this._router.navigate(['/login']);
        return false;
      })
    );
  }
}

