export class PreOrderCreateModel {
    public locationFromLat: number;
    public locationFromLng: number;
    public locationFromAddress: string;
    public locationToLat: number;
    public locationToLng: number;
    public locationToAddress: string;
}
