// администратор заявки
export const depositAdministratorChoices = [
    {label: 'Продавец', value: 'seller'},
    {label: 'Электронная площадка', value: 'operator'},
    {label: 'Организатор торгов', value: 'organizer'}
];
