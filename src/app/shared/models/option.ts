export class Option {
    public id: any;
    public description: string;
    public isDefault: boolean;
    public value: string;
}
